%% Inizialization
close all;

%%
Pltdim = 'km';

%%
figure
hold on; grid on;

PlotOrbit( X_ode .*1e-3 , 'LineWidth' , SA );
% PlotOrbit( r_site_eci , 'LineWidth' , SA );

[ PlotBody ] = PlotSolarSystem( 'EARTH' );

% Legend
Plt_TextLeg = { 
    'Chaser orbit' 
    };

% Plot Setting
xlabel( 'x [km]' );
ylabel( 'y [km]' );
zlabel( 'z [km]' );
legend('Chaser orbit' );
% PlotSet3D( 'x' , Pltdim , ...
%            'y' , Pltdim , ...
%            'z' , Pltdim , ...
%            Plt_TextLeg );
axis equal

% xlim auto
% ylim auto
% zlim auto

%%
lat_gd_arr = (180/pi) * ([ChaserCar.latgd].')';
lon_arr = (180/pi) * ([ChaserCar.lon].')';

% FigHandle = figure;
% hold on; grid on;
% 
% plotPlanisphere( FigHandle )
% [ PlotGroundTrack ] = plotGroundTrack( lat_gd_arr , lon_arr , 'r' , SA );

%%
PlotOrbitRadius( t_ode ./ ( 3600 ) , X_ode' .* 1e-3, { 'h' , 'km' } )
% PlotOrbitRadius( t_ode_s ./ ( 3600 ) , X_ode_s' , { 'h' , 'km' } )

%%
PlotOrbitVelocity( t_ode ./ ( 3600 ) , X_ode' .*1e-3, { 'h' , 'km' } )
% PlotOrbitVelocity( t_ode_s ./ ( 3600 ) , X_ode_s' , { 'h' , 'km' } )

%%
GM = Param.OrbitalMech.MainAttractor.GM;
PlotKeplerianElements( t_ode , X_ode .*1e-3, GM .*1e-9 );

%%
figure;
subplot( 1 , 3 , 1 )
hold on; grid on;
plot( t_ode/3600 , (180/pi) * ([ChaserCar.latgd].') , 'LineWidth' , SA )
plot( t_ode_s/3600 , (180/pi) * ([ObsCar.latgd].') , 'r' , 'LineWidth' , SA )
% PlotSet2D( ...
%     't' , 'h' , ...
%     '\phi_{gd} - Latitude' , 'deg' );
xlabel( 't [h]' );
ylabel( '\phi_{gd} - Latitude [deg]' );

subplot( 1 , 3 , 2 )
hold on; grid on;
plot( t_ode/3600 , (180/pi) * ([ChaserCar.lon].') , 'LineWidth' , SA )
plot( t_ode_s/3600 , (180/pi) * ([ObsCar.lon].') , 'r' , 'LineWidth' , SA )
% PlotSet2D( ...
%     't' , 'h' , ...
%     '\lambda - Longitude' , 'deg' );
xlabel( 't [h]' );
ylabel( '\lambda - Longitude [deg]' );

subplot( 1 , 3 , 3 )
hold on; grid on;
plot( t_ode/3600 , ([ChaserCar.alt].').*1e-3  , 'LineWidth' , SA )
plot( t_ode_s/3600 , ([ObsCar.alt].').*1e-3 , 'r' , 'LineWidth' , SA )
% PlotSet2D( ...
%     't' , 'h' , ...
%     'h' , 'km' );
xlabel( 't [h]' );
ylabel( 'h [deg]' );

%%
PlotOrbitRadius( Data_los.t ./ ( 3600 ) , Data_los.r' , { 'h' , '-' } )

%%
figure
subplot( 1 , 2 , 1 )
hold on; grid on;
plot( t_ode_s/3600 , (180/pi) * ([ObsCar.rtasc].')  , 'LineWidth' , SA )
% plot( t_ode_s/3600 , rad2deg([ObsCar.rtasc_s].') , 'r' , 'LineWidth' , SA )
% PlotSet2D( ...
%     't' , 'h' , ...
%     '\alpha_{t} - Topocentric Right Ascension' , 'deg' );
xlabel( 't [h]' );
ylabel( '\alpha_{t} - Topocentric Right Ascension [deg]' );

subplot( 1 , 2 , 2 )
hold on; grid on;
plot( t_ode_s/3600 , (180/pi) * ([ObsCar.decl].')  , 'LineWidth' , SA )
% plot( t_ode_s/3600 , rad2deg([ObsCar.decl_s].') , 'r' , 'LineWidth' , SA )
% PlotSet2D( ...
%     't' , 'h' , ...
%     '\delta_{t} - Topocentric Declination' , 'deg' );
xlabel( 't [h]' );
ylabel( '\delta_{t} - Topocentric Declination [deg]' );
