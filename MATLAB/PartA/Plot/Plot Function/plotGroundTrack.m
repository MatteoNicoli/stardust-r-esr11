function [ PlotGroundTrack ] = plotGroundTrack( Latitude , Longitude , Color , Linewidth )
%%
if nargin == 2
    
    Color = 'r';
    Linewidth = 1;
    
elseif nargin == 3
    
    Linewidth = 1;
    
end

%%
NCase1 = 0;
NCase2 = 0;
for iLong = 1 : length( Longitude ) - 1
    if Longitude( iLong ) > Longitude( iLong + 1 )
        NCase1 = NCase1 + 1;
        ilong1( NCase1 ) = iLong;
    elseif Longitude( iLong ) < Longitude( iLong + 1 )
        NCase2 = NCase2 + 1;
        ilong2( NCase2 ) = iLong;
    end
end

if NCase1 < NCase2
    NCase = NCase1;
    ilong = ilong1;
else
    NCase = NCase2;
    ilong = ilong2;
end


Long = zeros( 1 , size( Longitude , 2 ) + NCase );
Lat = zeros( 1 , size( Longitude , 2 ) + NCase );

Long = [ Longitude( 1 : ilong( end ) ) , NaN , Longitude( ilong( end ) + 1 : end ) ];
Lat = [ Latitude( 1 : ilong( end ) ) , NaN , Latitude( ilong( end ) + 1 : end ) ];
            
for iLong = NCase - 1 : -1 : 1
    Long = [ Long( 1 : ilong( iLong ) ) , NaN , Long( ilong( iLong ) + 1 : end ) ];
    Lat = [ Lat( 1 : ilong( iLong ) ) , NaN , Lat( ilong( iLong ) + 1 : end ) ];
end

%%
% geoshow(Lat,Long)
PlotGroundTrack = plot( Long(1) , Lat(1) , '*r' , 'Linewidth' , Linewidth );
PlotGroundTrack = plot( Long , Lat , Color , 'Linewidth' , Linewidth );

ax = gca;
ax.XLim = [ -180 , 180 ];
ax.YLim = [ -90 , 90 ];

end