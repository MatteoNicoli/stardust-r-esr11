%{
%-Abstract
%
%   PLOT3SETORBIT: This function sets the plot3 layout for the orbit 
%                  rapresentation.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      x:           x variable name.
%      xdim:        x dimension name.
%      y:           y variable name.
%      ydim:        y dimension name.
%      z:           z variable name.
%      zdim:        z dimension name.
%      varargin:    others parameter:
%                       cell whith name of legend variable.
%                       'Location' of the legend.
%                       'Orientation' of the legend.
%
%   The call:
%      
%      Plot3SetOrbit( x , xdim , y , ydim , z , zdim , varargin );
%
%   Returns:
%
%      values:   anything.
%
%-Version
%
%   -Version 1.0.0, 13-FEB-2018.
%
%-&
%}

function Plot3SetOrbit( x , xdim , y , ydim , z , zdim , varargin )

% Plot Setting
LabelFS = 18;
LegFS = 14;
Loc = 'NorthEast';
Ornt = 'Vertical';

LegPlotFlag = 0;
LegLabel = { '$\underline{r}(t)$' };

nVar = length( varargin );
for iVar = 1 : nVar
    Var = varargin{ iVar };
    Case = whos( 'Var' );
    Class = Case.class;
    switch Class
        case { 'cell' }
            Leglabel = Var;
            nLegLabel = length( Leglabel );
            for iLegLabel = 1 : nLegLabel
                LegLabel{ iLegLabel } = [ '$' Leglabel{ iLegLabel } '$' ];
            end
        case { 'matlab.graphics.chart.primitive.Line' }
            LegPlot = Var;
            LegPlotFlag = 1;
        case { 'char' }
            switch Var
                case { 'Location' , 'location' }
                    Loc = varargin{ iVar + 1 };
                case { 'Orientation' , 'orientation' }
                    Ornt = varargin{ iVar + 1 };
                otherwise
            end
        otherwise
    end
end

% Setting
xLabel = [ '$' x '$ $[' xdim ']$' ];
yLabel = [ '$' y '$ $[' ydim ']$' ];
zLabel = [ '$' z '$ $[' zdim ']$' ];
xlabel( xLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
ylabel( yLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
zlabel( zLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );

if LegPlotFlag == 0
    LegHandle = legend( LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
else
    LegHandle = legend( LegPlot , LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
end

set( LegHandle , ...
     'Interpreter' , 'LaTex' , ...
     'FontSize' , LegFS );
title( LegHandle , '$Legend$' , ...
       'Interpreter' , 'LaTex' , ...
       'FontSize' , LegFS * 16 / 15 );

end