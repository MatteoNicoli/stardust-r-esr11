function PlotKeplerianElements( t_ode , State_ode , GM )
SA = ( 1 + sqrt( 5 ) ) / 2;

%%
% GM = Param.OrbitalMech.MainAttractor.GM;
KE = zeros( length( t_ode ) , 6 );
for it = 1 : length( t_ode )
    r = State_ode( it , 1 : 3 );
    v = State_ode( it , 4 : 6 );
    [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ] = rv2coe (r,v,GM);
%     [ iKE , iTA ] = car2kep( State_ode( it , : ) , GM );
    KE( it , : ) = [ a,ecc,incl,omega,argp , nu ];
end

%%
figure;
subplot( 2 , 3 , 1 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) ,KE( : , 1 ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'a(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            'a' , 'km' );
xlabel( [ 't [h]' ] );
ylabel( [ 'a [km]' ] );
% axis equal

%%
% figure;
subplot( 2 , 3 , 2 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) ,KE( : , 2 ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'e(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            'e' , '-' );
xlabel( [ 't [h]' ] );
ylabel( [ 'e [-]' ] );
% axis equal

%%
% figure;
subplot( 2 , 3 , 3 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) , (180/pi) *( KE( : , 3 ) ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'i(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            'i' , 'deg' );
xlabel( [ 't [h]' ] );
ylabel( [ 'i [deg]' ] );
% axis equal

%%
% figure;
subplot( 2 , 3 , 4 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) , (180/pi) *( KE( : , 4 ) ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    '\Omega(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            '\Omega' , 'deg' );
xlabel( [ 't [h]' ] );
ylabel( [ '\Omega [deg]' ] );
% axis equal

%%
% figure;
subplot( 2 , 3 , 5 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) , (180/pi) *( KE( : , 5 ) ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    '\omega(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            '\omega' , 'deg' );
xlabel( [ 't [h]' ] );
ylabel( [ '\omega [deg]' ] );
% axis equal

%%
% figure;
subplot( 2 , 3 , 6 );
hold on; grid on;

plot( t_ode ./ ( 3600 ) , (180/pi) *( KE( : , 6 ) ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    '\vartheta(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , 'h' , ...
%            '\vartheta' , 'deg' );
xlabel( [ 't [h]' ] );
ylabel( [ '\vartheta [deg]' ] );
% axis equal

end