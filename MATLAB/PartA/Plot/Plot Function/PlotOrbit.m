%{
%-Abstract
%
%   PLOTORBIT: This plot the orbit by state vector matrix in 3-D plot.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      X:           State vector matrix of the orbit.
%      varargin:    Specifies line properties using one or more Name, Value 
%                   pair arguments.
%
%   The call:
%      
%      [ PlotHandle ] = PlotOrbit( X , varargin );
%
%   Returns:
%
%      PlotHandle:  returns a column vector of chart line objects. Use h to
%                   modify properties of a specific chart line after it is 
%                   created.
%
%-Version
%
%   -Version 1.0.0, 13-FEB-2018.
%
%-&
%}

function [ varargout ] = PlotOrbit( X , varargin )
[ row , col ] = size( X );
nist = row;
if col == 6 || col == 3
    X = X';
    nist = col;
elseif col == 4 || col == 2
    X = X';
    nist = col;
end

if nist == 2 || nist == 4
    X0 = zeros( 1 , size( X , 2 ) );
    X = [ X( 1 : 2 , : ) ; X0 ; X( 3 : 4 , : ) ; X0 ];
end

% if nist == 3 || nist == 6
    
    Nvararigin = length( varargin );
    if nargin == 1
        PlotHandle = plot3( X( 1 , : ) , X( 2 , : ) , X( 3 , : ) );
    elseif Nvararigin == 1
        PlotHandle = plot3( X( 1 , : ) , X( 2 , : ) , X( 3 , : ) , varargin{ 1 } );
    else
        if mod( Nvararigin , 2 ) == 1
            PlotHandle = plot3( X( 1 , : ) , X( 2 , : ) , X( 3 , : ) , varargin{ 1 } );
            for iInput = 2 : Nvararigin - 1
                if mod( iInput , 2 ) == 0
                    set( PlotHandle , varargin{ iInput } , varargin{ iInput + 1 } );
                end
            end
        else
            PlotHandle = plot3( X( 1 , : ) , X( 2 , : ) , X( 3 , : ) );
            for iInput = 1 : Nvararigin - 1
                if mod( iInput , 2 ) == 1
                    set( PlotHandle , varargin{ iInput } , varargin{ iInput + 1 } );
                end
            end
        end
    end
    
% elseif nist == 2 || nist == 4
    
%     Nvararigin = length( varargin );
%     if nargin == 1
%         PlotHandle = plot( X( 1 , : ) , X( 2 , : ) );
%     elseif Nvararigin == 1
%         PlotHandle = plot( X( 1 , : ) , X( 2 , : ) , varargin{ 1 } );
%     else
%         if mod( Nvararigin , 2 ) == 1
%             PlotHandle = plot( X( 1 , : ) , X( 2 , : ) , varargin{ 1 } );
%             for iInput = 2 : Nvararigin - 1
%                 if mod( iInput , 2 ) == 0
%                     set( PlotHandle , varargin{ iInput } , varargin{ iInput + 1 } );
%                 end
%             end
%         else
%             PlotHandle = plot( X( 1 , : ) , X( 2 , : ) );
%             for iInput = 1 : Nvararigin - 1
%                 if mod( iInput , 2 ) == 1
%                     set( PlotHandle , varargin{ iInput } , varargin{ iInput + 1 } );
%                 end
%             end
%         end
%     end
    
%     [ PlotHandle ] = PlotOrbit( X , varargin );
% end

varargout{ 1 } = PlotHandle;

end