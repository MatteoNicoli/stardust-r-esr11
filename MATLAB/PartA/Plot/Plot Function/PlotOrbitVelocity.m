function PlotOrbitVelocity( t_ode , State_ode , si )
SA = ( 1 + sqrt( 5 ) ) / 2;

figure;
subplot( 1 , 2 , 1 );
hold on; grid on;

plot( t_ode , State_ode( 4 : 6 , : ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'v_x(t)' , ...
    'v_y(t)' , ...
    'v_z(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , si{ 1 } , ...
%            'v' , [ si{ 2 } '/s' ] , ...
%            Plt_TextLeg );
xlabel( [ 't ' si{ 1 } ] );
ylabel( [ 'v ' si{ 2 } ] );
legend([ 'v_x(t)' , ...
    'v_y(t)' , ...
    'v_z(t)' ])
% axis equal

%%
v_plot = sqrt( sum( State_ode( 4 : 6 , : ).^2 , 1 ) );
subplot( 1 , 2 , 2 );
hold on; grid on;

plot( t_ode , v_plot , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'v(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , si{ 1 } , ...
%            'v' , [ si{ 2 } '/s' ] , ...
%            Plt_TextLeg );
xlabel( [ 't ' si{ 1 } ] );
ylabel( [ 'v ' si{ 2 } ] );
legend(['v(t)'])
% axis equal

end