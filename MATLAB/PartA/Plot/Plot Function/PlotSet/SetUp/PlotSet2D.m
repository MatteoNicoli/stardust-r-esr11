%{
%-Abstract
%
%   PLOTSET2D: This function sets the 2D plot layout.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      x:           x variable name.
%      xdim:        x dimension name.
%      y:           y variable name.
%      ydim:        y dimension name.
%      varargin:    others parameter:
%                       cell whith name of legend variable.
%                       'Location' of the legend.
%                       'Orientation' of the legend.
%
%   The call:
%      
%      PlotSet2D( x , xdim , y , ydim , varargin );
%
%   Returns:
%
%      values:   anything.
%
%-Version
%
%   -Version 1.0.0, 10-FEB-2018.
%
%-&
%}

function PlotSet2D( x , xdim , y , ydim , varargin )

% Plot Setting
GCA = gca;
set( gca , 'TickLabelInterpreter' , 'LaTex' )

LabelFS = GCA.FontSize;
LegFS = GCA.FontSize;
% LabelFS = 16;
% LegFS = 14;
Loc = 'NorthEast';
Ornt = 'Vertical';

% LegPlotFlag = 0;
% LegLabel = { [ '$' y '(' x ')$' ] };

nVar = length( varargin );
for iVar = 1 : nVar
    Var = varargin{ iVar };
    Case = whos( 'Var' );
    Class = Case.class;
    switch Class
        case { 'cell' }
            Leglabel = Var;
            nLegLabel = length( Leglabel );
            for iLegLabel = 1 : nLegLabel
                LegLabel{ iLegLabel } = [ '$' strrep( Leglabel{ iLegLabel } , ' ' , ' \; ' ) '$' ];
            end
%             LegPlotFlag = 1;
        case { 'matlab.graphics.chart.primitive.Line' }
            LegPlot = Var;
%             LegPlotFlag = 2;
        case { 'char' }
            str = lower( Var );
            switch str
                case { 'location' }
                    Loc = varargin{ iVar + 1 };
                case { 'orientation' }
                    Ornt = varargin{ iVar + 1 };
                otherwise
%                     error( 'error: input variable (varargin)' );
            end
        otherwise
%             error( 'error: input variable (varargin)' );
    end
end

% Setting
% xLabel = [ '$' strrep( x , ' ' , ' \; ' ) '$ $[' strrep( xdim , ' ' , ' \; ' ) ']$' ];
% yLabel = [ '$' strrep( y , ' ' , ' \; ' ) '$ $[' strrep( ydim , ' ' , ' \; ' ) ']$' ];
% xlabel( xLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
% ylabel( yLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );

xLabel = [ '$' strrep( x , ' ' , ' \; ' ) ' \; [$' xdim '$]$' ];
yLabel = [ '$' strrep( y , ' ' , ' \; ' ) ' \; [$' ydim '$]$' ];
xlabel( xLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
ylabel( yLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );

if exist( 'LegLabel' , 'var' ) == 1 && exist( 'LegPlot' , 'var' ) == 1
    LegHandle = legend( LegPlot , LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
    set( LegHandle , ...
         'Interpreter' , 'LaTex' , ...
         'FontSize' , LegFS );
    title( LegHandle , '$Legend$' , ...
           'Interpreter' , 'LaTex' , ...
           'FontSize' , LegFS * 16 / 15 );
elseif exist( 'LegLabel' , 'var' ) == 1

    LegHandle = legend( LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
    set( LegHandle , ...
         'Interpreter' , 'LaTex' , ...
         'FontSize' , LegFS );
    title( LegHandle , '$Legend$' , ...
           'Interpreter' , 'LaTex' , ...
           'FontSize' , LegFS * 16 / 15 );
end

end