%{
%-Abstract
%
%   PLOTSET3D: This function sets the plot3 layout for the orbit 
%              rapresentation.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      x:           x variable name.
%      xdim:        x dimension name.
%      y:           y variable name.
%      ydim:        y dimension name.
%      z:           z variable name.
%      zdim:        z dimension name.
%      varargin:    others parameter:
%                       cell whith name of legend variable.
%                       'Location' of the legend.
%                       'Orientation' of the legend.
%
%   The call:
%      
%      PlotSet3D( x , xdim , y , ydim , z , zdim , varargin );
%
%   Returns:
%
%      values:   anything.
%
%-Version
%
%   -Version 1.0.0, 13-FEB-2018.
%
%-&
%}

function PlotSet3D( x , xdim , y , ydim , z , zdim , varargin )

% Plot Setting
GCA = gca;
set( gca , 'TickLabelInterpreter' , 'LaTex' )

LabelFS = GCA.FontSize;
LegFS = GCA.FontSize;
% LabelFS = 16;
% LegFS = 14;
Loc = 'NorthEast';
Ornt = 'Vertical';

% LegPlotFlag = 0;
% LegLabel = { '$z(x,y)$' };

nVar = length( varargin );
for iVar = 1 : nVar
    Var = varargin{ iVar };
    Case = whos( 'Var' );
    Class = Case.class;
    switch Class
        case { 'cell' }
            Leglabel = Var;
            nLegLabel = length( Leglabel );
            for iLegLabel = 1 : nLegLabel
                LegLabel{ iLegLabel } = [ '$' strrep( Leglabel{ iLegLabel } , ' ' , ' \; ' ) '$' ];
            end
%             LegPlotFlag = 1;
        case { 'matlab.graphics.chart.primitive.Line' }
            LegPlot = Var;
%             LegPlotFlag = 2;
        case { 'char' }
            str = lower( Var );
            switch str
                case { 'location' }
                    Loc = varargin{ iVar + 1 };
                case { 'orientation' }
                    Ornt = varargin{ iVar + 1 };
                otherwise
%                     error( 'error: input variable (varargin)' );
            end
        otherwise
%             error( 'error: input variable (varargin)' );
    end
end

% Setting
% xLabel = [ '$' strrep( x , ' ' , '$ $' ) '$ $[' strrep( xdim , ' ' , ' \; ' ) ']$' ];
% yLabel = [ '$' strrep( y , ' ' , '$ $' ) '$ $[' strrep( ydim , ' ' , ' \; ' ) ']$' ];
% zLabel = [ '$' strrep( z , ' ' , '$ $' ) '$ $[' strrep( zdim , ' ' , ' \; ' ) ']$' ];
xLabel = [ '$' strrep( x , ' ' , ' \; ' ) ' \; [$' xdim '$]$' ];
yLabel = [ '$' strrep( y , ' ' , ' \; ' ) ' \; [$' ydim '$]$' ];
zLabel = [ '$' strrep( z , ' ' , ' \; ' ) ' \; [$' zdim '$]$' ];
xlabel( xLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
ylabel( yLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );
zlabel( zLabel , 'Interpreter' , 'LaTex' , 'FontSize' , LabelFS );

if exist( 'LegLabel' , 'var' ) == 1 && exist( 'LegPlot' , 'var' ) == 1
    LegHandle = legend( LegPlot , LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
    set( LegHandle , ...
         'Interpreter' , 'LaTex' , ...
         'FontSize' , LegFS );
    title( LegHandle , '$Legend$' , ...
           'Interpreter' , 'LaTex' , ...
           'FontSize' , LegFS * 16 / 15 );
elseif exist( 'LegLabel' , 'var' ) == 1
    LegHandle = legend( LegLabel , ...
                        'Location' , Loc , ...
                        'Orientation' , Ornt );
    set( LegHandle , ...
         'Interpreter' , 'LaTex' , ...
         'FontSize' , LegFS );
    title( LegHandle , '$Legend$' , ...
           'Interpreter' , 'LaTex' , ...
           'FontSize' , LegFS * 16 / 15 );
end

end