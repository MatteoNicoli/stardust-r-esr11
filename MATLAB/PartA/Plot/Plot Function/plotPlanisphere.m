function [  ] = plotPlanisphere( FigHandle , NameImagin )
%%
if nargin == 1
    
    NameImagin = 'Earth2.jpg';
    
end

%%
% log = - 180 : 15 : 180;
% for i = 1 : length( log )
%     
%     if log( i ) < 0
%         
%         XLabelLong( i ) = { [ num2str( abs( log( i ) ) ) ' W' ] };
%         
%     else
%         
%         XLabelLong( i ) = { [ num2str( abs( log( i ) ) ) ' E' ] };
%         
%     end
%     
% end
% 
% lat = - 90 : 15 : 90;
% for i = 1 : length( lat )
%     
%     if lat( i ) < 0
%         
%         YLabelLat( i ) = { [ num2str( abs( lat( i ) ) ) , ' S' ] };
%         
%     else
%         
%         YLabelLat( i ) = { [ num2str( abs( lat( i ) ) ) ' N' ] };
%         
%     end
%     
% end

%%
% xwidth = 820;
% ywidth = 420;
% hFig = figure(1);
%  set(gcf,'PaperPositionMode','auto')
%  set(hFig, 'Position', [100 100 xwidth ywidth])
% hold on;
% grid on;
% axis([-180 180 -90 90]);
% load('topo.mat','topo','topomap1');
% contour(-359:0,-89:90,topo,[0 0],'b')
% contour(0:359,-89:90,topo,[0 0],'b')
% axis equal
% box on
% image([-360 0],[-90 90],topo,'CDataMapping', 'scaled');
% image([0 360],[-90 90],topo,'CDataMapping', 'scaled');
% colormap(topomap1);

%%
img = imread( NameImagin );
imshow( img , 'Xdata' , [ -180 180 ] , 'Ydata' , [ 90 -90 ] );%, 'Border', 'loses' , 'InizialMagnification' , 'fit' )
% imagesc( - 180 : 180 , 90 : - 1 : - 90 , rgb );
% geoshow( NameImagin )

hold on; grid on; axis on;
title( '$Ground$ $Track$' , 'Interpreter' , 'Latex' );
ylabel( '$\phi_{gd}$ $-$ $Latitude$ [deg]' , 'Fontsize' , 12 , 'Interpreter' , 'Latex' );
xlabel( '$\lambda$ $-$ $Longitude$ [deg]' , 'Fontsize' , 12 , 'Interpreter' , 'Latex' );

ax = gca;
ax.TickLabelInterpreter = 'LaTex';
ax.YDir = 'normal';
ax.XLim = [ -180 , 180 ];
ax.YLim = [ -90 , 90 ];
ax.XTick = (-180:15:180);
ax.YTick = (-90:15:90);
xticklabels({'180 W','165 W','150 W','135 W','120 W','105 W','90 W','75 W','60 W','45 W','30 W','15 W','0','15 E','30 E','45 E','60 E','75 E','90 E','105 E','120 E','135 E','150 E','165 E','180 E'})
yticklabels({'90 S','75 S','60 S','45 S','30 S','15 S','0','15 N','30 N','45 N','60 N','75 N','90 N'})

% set( gca , 'Ydir' , 'Normal' );
% set( gca , 'XLim' , [ - 180 , 180 ] , 'YLim' , [ - 90 , 90 ] , ...
%     'XTick' , - 180 : 15 : 180 , ...
%     'Ytick' , - 90 : 15 : 90 );
% ax = axesm('mercator');
% Axes = axes('Parent',FigHandle,'XTickLabel',XLabelLong,'YTickLabel',YLabelLat);
% box(Axes,'on');
% hold(Axes,'all');

end