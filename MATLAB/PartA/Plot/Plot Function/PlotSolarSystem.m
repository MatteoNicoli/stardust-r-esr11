function [ PlotBody , PlotQuiver ] = PlotSolarSystem( ChoiseBody , RotateParm , CenterCoordinate , QuiverColor )
%{
%% Dati in Entrata: -ChoiseBody:
%
%}
%% Scelta
switch ChoiseBody
    
    case { 'SUN' , 'Sun' , 'sun' }
        
        BODY = imread( 'Sun.png' , 'png' );
%         R_phi_equa = 695700;
%         R_phi_polar = 695700;
        
    case { 'MERCURY' , 'Mercury' , 'mercury' }
        
        BODY = imread( 'Mercury.jpg' , 'jpg' );
%         R_phi_equa = 2439.7;
%         R_phi_polar = 2439.7;
        
    case { 'VENUS' , 'Venus' , 'venus' }
        
        BODY = imread( 'Venus.jpg' , 'jpg' );
%         R_phi_equa = 6051.8;
%         R_phi_polar = 6051.8;
        
    case { 'EARTH' , 'Earth' , 'earth' }
        
        BODY = imread( 'Earth2.jpg' , 'jpg' );
        RADII = [ 6378136.3 ; 6378136.3 ; 6356751.60056294 ]* 1e-3;
        
    case { 'MOON' , 'Moon' , 'moon' }
        
        BODY = imread( 'Moon.png' , 'png' );
%         R_phi_equa = 1738.1;
%         R_phi_polar = 1736.0;
        
    case { 'MARS' , 'Mars' , 'mars' }
        
        BODY = imread( 'Mars.jpg' , 'jpg' );
%         R_phi_equa = 3396.2;
%         R_phi_polar = 3376.2;
        
    case { 'JUPITER' , 'Jupiter' , 'jupiter' }
        
        BODY = imread( 'Jupiter.jpg' , 'jpg' );
%         R_phi_equa = 142984;
%         R_phi_polar = 133709;
        
    case { 'IO' , 'Io' , 'io' , 'JI' }
        
        BODY = imread( 'Io.jpg' , 'jpg' );
%         R_phi_equa = 1821.6;
%         R_phi_polar = 1821.6;
        
    case { 'EUROPA' , 'Europa' , 'europa' , 'JII' }
        
        BODY = imread( 'Europa.jpg' , 'jpg' );
%         R_phi_equa = 1565;
%         R_phi_polar = 1565;
        
    case { 'GANYMEDE' , 'Ganymede' , 'ganymede' , 'JIII' }
        
        BODY = imread( 'Ganymede.png' , 'png' );
%         R_phi_equa = 2631.2;
%         R_phi_polar = 2631.2;
        
    case { 'CALLISTO' , 'Callisto' , 'callisto' , 'JIV' }
        
        BODY = imread( 'Callisto.jpg' , 'jpg' );
%         R_phi_equa = 2410.3;
%         R_phi_polar = 2410.3;
        
    case { 'SATURN' , 'Saturn' , 'saturn' }
        
        BODY = imread( 'Saturn.jpg' , 'jpg' );
%         R_phi_equa = 60268;
%         R_phi_polar = 54364;
        
    case { 'URANUS' , 'Uranus' , 'uranus' }
        
        BODY = imread( 'Uranus.jpg' , 'jpg' );
%         R_phi_equa = 25559;
%         R_phi_polar = 24973;
        
    case { 'NEPTUNE' , 'Neptune' , 'neptune' }
        
        BODY = imread( 'Neptune.jpg' , 'jpg' );
%         R_phi_equa = 24764;
%         R_phi_polar = 24341;
        
    case { 'PLUTO' , 'Pluto' , 'pluto' }
        
        BODY = imread( 'Pluto.jpg' , 'jpg' );
%         R_phi_equa = 1187;
%         R_phi_polar = 1187;
        
    otherwise
        
        error( 'Input Error ( Body Choise )' );
        
end

%% Plot the Body
if nargin < 3
    CenterCoordinate = [ 0 , 0 , 0 ];
end

% RADII = cspice_bodvrd( ChoiseBody , 'RADII' , 3 ) * 1e3;

props.FaceColor = 'texture';
props.EdgeColor = 'none';
props.FaceLighting = 'phong';
props.Cdata = BODY;
[ XX , YY , ZZ ] = ellipsoid( - CenterCoordinate( 1 ) , - CenterCoordinate( 2 ) ,...
    - CenterCoordinate( 3 ) , RADII( 1 ) , RADII( 2 ) , RADII( 3 ) , 30 );

hold on; %axis equal
[ PlotBody ] = surface( - XX , - YY , - ZZ , props );

%% Body Rotating
if nargin > 1
    
    R_Eque2RF = cspice_sxform( RotateParm.RFfrom , RotateParm.RFto , RotateParm.t );
    k = [ 0 ; 0 ; 1 ; 0 ; 0 ; 0 ];
    np = R_Eque2RF * k;
    K = k( 1 : 3 );
    NP = np( 1 : 3 );
    
    RotateAxis = cross( K , NP ) / norm( cross( K , NP ) );
    Angle = acosd( dot( K , NP ) / ( norm( K ) * norm( NP ) ) );
    rotate( PlotBody , RotateAxis , Angle );

else
    
    NP = [ 0 ; 0 ; 1 ];
    
end

%% North Pole Quiver
if nargin > 3
    
    NP = RADII( 3 ) * 2 * NP;
    PlotQuiver = quiver3( 0 , 0 , 0 , NP( 1 ) , NP( 2 ) , NP( 3 ) , QuiverColor , 'linewidth' , 2 );
    
end

end