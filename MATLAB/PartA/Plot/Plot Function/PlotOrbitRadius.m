function PlotOrbitRadius( t_ode , State_ode , si )
SA = ( 1 + sqrt( 5 ) ) / 2;

%% Radius.
figure;
subplot( 1 , 2 , 1 );
hold on; grid on;

plot( t_ode , State_ode( 1 : 3 , : ) , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'r_x(t)' , ...
    'r_y(t)' , ...
    'r_z(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , si{ 1 } , ...
%            'r' , si{ 2 } , ...
%            Plt_TextLeg );
xlabel( [ 't ' si{ 1 } ] );
ylabel( [ 'r ' si{ 2 } ] );
legend([    'r_x(t)' , ...
    'r_y(t)' , ...
    'r_z(t)' ])
% axis equal

%%
r_plot = sqrt( sum( State_ode( 1 : 3 , : ).^2 , 1 ) );
subplot( 1 , 2 , 2 );
hold on; grid on;

plot( t_ode , r_plot , 'LineWidth' , SA );

% Legend
Plt_TextLeg = { 
    'r(t)' , ...
    };

% Plot Setting
% PlotSet2D( 't' , si{ 1 } , ...
%            'r' , si{ 2 } , ...
%            Plt_TextLeg );
xlabel( [ 't ' si{ 1 } ] );
ylabel( [ 'r ' si{ 2 } ] );
legend(['r(t)'])
% axis equal

end