%% Installing
clear all;
close all;
clc;

%%
[ FullPath ]= mfilename( 'fullpath' );
[ CurrentPathStr , ScriptName ] = fileparts( FullPath );
addpath( genpath( CurrentPathStr ) );

%%
OSType = computer;
switch OSType( 1 : end - 2 )
    case { 'PCWIN' }
%         CurrentDir = CurrentFold( 3 : end );
        SpF = '\';
    case { 'GLNXA' }
%         CurrentDir = CurrentFold;
        SpF = '/';
    case { 'MACI' }
        SpF = '/';
end

%%
clear;
clc;
