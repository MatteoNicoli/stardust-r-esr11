% clear; clc;
%%
SA = ( 1 + sqrt( 5 ) ) / 2;
constmath

%% Data
% Celestial Bodies Charateristics.
[ Param.OrbitalMech , Const ] = OrbitalMechCharateristics;

%%
Param.OrbitalMech.CoorTime.noiserng = 0.0925*1e3;
Param.OrbitalMech.CoorTime.noiseaz = 0.0224 / Const.Deg;
Param.OrbitalMech.CoorTime.noiseel = 0.0139 / Const.Deg;
Param.OrbitalMech.CoorTime.noisetrtasc = 0.0224 / Const.Deg;
Param.OrbitalMech.CoorTime.noisetdecl = 0.0139 / Const.Deg;
% Param.OrbitalMech.CoorTime.obstype = 2;


mu = Param.OrbitalMech.MainAttractor.GM;
GM = Param.OrbitalMech.MainAttractor.GM;
re = Param.OrbitalMech.MainAttractor.R;
flat = Param.OrbitalMech.MainAttractor.Flat;

%%
global Cnm Snm AuxParam eopdata

eopdata = Param.OrbitalMech.eopdata;
MJD_J2000 = 51544.5;             % Modif. Julian Date of J2000.0

Cnm = zeros(71,71);
Snm = zeros(71,71);
fid = fopen('ITG-Grace03s.txt','r');
for n=0:70
    for m=0:n
        temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);        
        Cnm(n+1,m+1) = temp(3);
        Snm(n+1,m+1) = temp(4);
    end
end

fclose(fid);

%%
data_los = importdata('losMeasurements.csv');
Data_los.t = data_los( : , 1 );
Data_los.r = data_los( : , 2 : 4 );

%%
for it = 1 : length(Data_los.t)
    [ ~ , i_decl_t , i_rtasc_t ] = car2sph( Data_los.r(it,1) , Data_los.r(it,2) , Data_los.r(it,3) );
    rtascarr(it,1) = i_rtasc_t;
    declarr(it,1) = i_decl_t;
    rtasc_t(it,1) = i_rtasc_t;
    decl_t(it,1) = i_decl_t;
    
end

i_t = 0;
it_obs_slot = [ 1 ];
for it = 1 : length(Data_los.t) - 1
    if Data_los.t( it + 1 ) - Data_los.t( it ) > 4 * 60
        i_t = i_t + 1;
        it_obs_slot = [ it_obs_slot , it + 1 ];
    end
end
it_obs_slot = [ it_obs_slot , length(Data_los.t) ];

%% End of Data file
