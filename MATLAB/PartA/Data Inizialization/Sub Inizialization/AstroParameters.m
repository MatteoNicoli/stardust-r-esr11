%{
%-Abstract
%
%   ASTROPARAMETERS: This function returns the most important parameters of
%                    a body, for example GM and ray, given the name of the 
%                    body.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      BodyID:	Char name or ID of the body that interest the charateristics.
%
%   The call:
%      
%      [ BodyPar , AstroConst ] = AstroParameters( BodyID );
%
%   Returns:
%
%      BodyPar: Structure contain the charateristics of the body (i.e. 
%               name, ID, GM, rdius of body.
%      AstroConst: Astrodynamic Constants.
%
%-Version
%
%   -Version 1.0.0, 31-OCT-2017.
%
%-&
%}

function [ BodyPar , AstroConst ] = AstroParameters( BodyID )

FileType = whos( 'BodyID' );
switch FileType.class
    case { 'char' }
        BodyName = lower( BodyID );
    case { 'double' }
        BodyName = num2str( BodyID );
    otherwise
        error( 'error Body name or ID (BodyID)' );
end

%% Astrodynamic Constants.
AstroConst.Jd  = 24 * 3600;
AstroConst.Jy  = 365.25 * AstroConst.Jd;
AstroConst.Jc  = 36525 * AstroConst.Jd;
AstroConst.c   = 299792457.999999984;
AstroConst.AU  = 149597870691.000015;
AstroConst.MSd = 86164.09054;
AstroConst.Sy  = 365.25636 * AstroConst.Jd;
AstroConst.G   = 6.67259e-11;

%% Body Parameters
[ BodyPar ] = BodyParameters( BodyName );

%%
switch BodyPar.Name
    case { 'SUN' }
        BodyPar.L         = 3.828e26;    % Luminosity [W]
        BodyPar.EquAngle  = (pi/180) * ( 0 );
        omega             = 0;    % Mean angular velocity of the main attractor [rad/s].
        BodyPar.omega     = [ 0 ; 0 ; omega ];
        
    case { 'EARTH' }
        BodyPar.L         = 0;                        % Luminosity [W]
        BodyPar.EquAngle  = (pi/180) * ( 23.4392911111111 );
%         omega             = 7.29211585275553e-005;    % Mean angular velocity of the main attractor [rad/s].
        omega_Earth = 7.2921151467e-5;   % [rad/s]; WGS-84
        omega = omega_Earth;
        BodyPar.omega     = [ 0 ; 0 ; omega ];
        
        BodyPar.Cnm = zeros(71,71);
        BodyPar.Snm = zeros(71,71);
        fid = fopen('ITG-Grace03s.txt','r');
        for n=0:70
            for m=0:n
                temp = fscanf(fid,'%d %d %f %f %f %f',[6 1]);        
                BodyPar.Cnm(n+1,m+1) = temp(3);
                BodyPar.Snm(n+1,m+1) = temp(4);
            end
        end
        fclose(fid);
        
%         load('aeroegm2008.mat');
%         BodyPar.NC_nm = C;
%         BodyPar.NS_nm = S;
% %         BodyPar.NC_nm = importdata( 'EGM2008_NC.txt' );
% %         BodyPar.NS_nm = importdata( 'EGM2008_NS.txt' );
% %         invNGravCoefnm = importdata( 'invNGravCoefsnm.txt' );
%         [ R_Cnm , C_Cnm ] = size( BodyPar.NC_nm );
%         [ invNGravCoefnm ] = fcn_NGravCoefsnm( R_Cnm - 1 );
%         BodyPar.C_nm = BodyPar.NC_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
%         BodyPar.S_nm = BodyPar.NS_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
        
%         [ aI ] = GravityField_accI_25( r , phi , lambda , .R , .GM , C_nm , S_nm );
%         [ aJ ] = GravityField_accJ_25( r , phi , lambda , .R , .GM , C_nm , S_nm );
%         [ aK ] = GravityField_accK_25( r , phi , lambda , .R , .GM , C_nm , S_nm );


        %
%         fileID = 'igrf12coeffs.txt';
%         Data = importdata( fileID );
%         D = Data.data;
%         [ Dim ] = size( D );
% 
%         g = zeros( 13 , 14 );
%         h = g;
% 
%         flag = 0;
%         for igh = 1 : Dim( 1 )
%             if D( igh , 2 ) == 0
%                 g( D( igh , 1 ) , D( igh , 2 ) + 1 ) = D( igh , end - 1 ) * 1e-9;
%             elseif flag == 0
%                 g( D( igh , 1 ) , D( igh , 2 ) + 1 ) = D( igh , end - 1 ) * 1e-9;
%                 flag = 1;
%             elseif flag == 1
%                 h( D( igh , 1 ) , D( igh , 2 ) + 1 ) = D( igh , end - 1 ) * 1e-9;
%                 flag = 0;
%             end
%         end
% 
%         BodyPar.MagnField.g_nm = g;
%         BodyPar.MagnField.h_nm = h;
%         
%         [ BI ] = MagneticField_BI_12( [1,2,3],2,3 ,BodyPar.R , BodyPar.MagnField.g_nm , BodyPar.MagnField.h_nm );
%         [ BJ ] = MagneticField_BJ_12( BodyPar.R , BodyPar.MagnField.g_nm , BodyPar.MagnField.h_nm );
%         [ BK ] = MagneticField_BK_12( BodyPar.R , BodyPar.MagnField.g_nm , BodyPar.MagnField.h_nm );
%         
%         BodyPar.MagnField.B =@(r,phi,lambda) [ BI(r,phi,lambda) ; BJ(r,phi,lambda) ; BK(r,phi,lambda) ];
        
%         B_file;
%         
%         BodyPar.MagnField.B =@(r,phi,lambda) B(r,phi,lambda,BodyPar.R,g,h);
%         BodyPar.MagnField.B =B;
        
    case { 'MOON' }
        BodyPar.L         = 0;    % Luminosity [W]
        BodyPar.EquAngle  = (pi/180) * ( 0 );
        omega             = 0;    % Mean angular velocity of the main attractor [rad/s].
        BodyPar.omega     = [ 0 ; 0 ; omega ];
        
%         load('aerolp165p.mat');
%         BodyPar.NC_nm = C;
%         BodyPar.NS_nm = S;
% %         BodyPar.NC_nm = importdata( 'LP165P_NC.txt' );
% %         BodyPar.NS_nm = importdata( 'LP165P_NS.txt' );
% %         invNGravCoefnm = importdata( 'invNGravCoefsnm.txt' );
%         [ R_Cnm , C_Cnm ] = size( BodyPar.NC_nm );
%         [ invNGravCoefnm ] = fcn_NGravCoefsnm( R_Cnm - 1 );
%         BodyPar.C_nm = BodyPar.NC_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
%         BodyPar.S_nm = BodyPar.NS_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
        
%         BodyPar.MagnField.g = g;
%         BodyPar.MagnField.h = h;
        
    case { 'MARS' }
        BodyPar.L         = 0;    % Luminosity [W]
        BodyPar.EquAngle  = (pi/180) * ( 0 );
        omega             = 0;    % Mean angular velocity of the main attractor [rad/s].
        BodyPar.omega     = [ 0 ; 0 ; omega ];

%         load('aerogmm2b.mat')
%         BodyPar.NC_nm = C;
%         BodyPar.NS_nm = S;
% %         BodyPar.NC_nm = importdata( 'GMM2B_NC.txt' );
% %         BodyPar.NS_nm = importdata( 'GMM2B_NS.txt' );
% %         invNGravCoefnm = importdata( 'invNGravCoefsnm.txt' );
%         [ R_Cnm , C_Cnm ] = size( BodyPar.NC_nm );
%         [ invNGravCoefnm ] = fcn_NGravCoefsnm( R_Cnm - 1 );
%         BodyPar.C_nm = BodyPar.NC_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
%         BodyPar.S_nm = BodyPar.NS_nm .* invNGravCoefnm( 1 : R_Cnm , 1 : C_Cnm );
        
%         BodyPar.MagnField.g = g;
%         BodyPar.MagnField.h = h;
end

end