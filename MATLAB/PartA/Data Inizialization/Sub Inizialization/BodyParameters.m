%{
%-Abstract
%
%   BODYPARAMETERS: This function returns the most important parameters of
%                   a body, for example GM and ray, given the name of the 
%                   body.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      BodyName:	Name of the body that interest the charateristics.
%
%   The call:
%      
%      [ Body ] = BodyParameters( BodyName );
%
%   Returns:
%
%      Body: Structure contain the charateristics of the body (i.e. name,
%            ID, GM, rdius of body.
%
%-Version
%
%   -Version 1.0.0, 31-OCT-2017.
%
%-&
%}

function [ Body ] = BodyParameters( BodyName )
dim = 1e3;

switch lower( BodyName )
        
    case { 'earth' }
        R_Earth   = 6378.137e3;          % Earth's radius [m]; WGS-84
        f_Earth   = 1.0/298.257223563;   % Flattening; WGS-84   
        GM_Earth   = 398600.4418e9;                % [m^3/s^2]; WGS-84
        
        Body.Name = 'EARTH';
        Body.ID   = 399;
        Body.GM   = GM_Earth;
%         Body.R3D  = [ 6378136.3 ; 6378136.3 ; 6356751.60056294 ]*1e-3;
        Body.R    = R_Earth;
        Body.Flat = f_Earth;
        
    otherwise
%         ID = cspice_bods2c( BodyName );
%         Body.Name = cspice_bodc2n( ID );                                % Name of the Body.
%         Body.ID   = ID;                                                 % ID of the Body.
%         Body.GM   = cspice_bodvrd( Body.Name , 'GM' , 1 ) .* dim.^3;    % Standard gravitational parameter for the i-th body [km^3/s^2].
%         Body.R3D  = cspice_bodvrd( Body.Name , 'RADII' , 3 ) .* dim;    % Radius of the i-th body [km].
%         Body.R    = mean( Body.R3D );                                   % Mean radius of the i-th body [km].
%         Body.Flat = ( Body.R3D( 1 ) - Body.R3D( 3 ) ) / Body.R3D( 1 );  % Flatness coefficient of the i-th body [].
end


end