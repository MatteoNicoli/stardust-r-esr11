%{
%-Abstract
%
%   ORBITALMECHCHARATERISTICS: This function returns the most important
%                              parameters of the celestial bodies, 
%                              reference frame and date.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      anything:	.
%
%   The call:
%      
%      [ OrbitalMech ] = OrbitalMechCharateristics;
%
%   Returns:
%
%      OrbitalMech: Structure contain the charateristics of the body which 
%                   i consider, reerence frame e date.
%
%-Version
%
%   -Version 1.0.0, 25-NOV-2017.
%
%-&
%}

function [ OrbitalMech , Const ] = OrbitalMechCharateristics
%% Constant
Const.pi2       = 2*pi;                % 2pi
Const.Rad       = pi/180;              % Radians per degree
Const.Deg       = 180/pi;              % Degrees per radian
Const.Arcs      = (3600*180)/pi;       % Arcseconds per radian
Const.Convrt    = pi/(3600*180);       % Radian per Arcseconds

%% Reference Frame and Abberation Correction.
OrbitalMech.RefCorr.RefFrame = 'J2000';
OrbitalMech.RefCorr.AbCorr = 'NONE';

%% Start Date
OrbitalMech.DateStart = '26-Feb-1996 07:40:00';
OrbitalMech.DateStart_vet = [ 1996 02 26 07 39 00 ];

% OrbitalMech.ET_MJD2000   = cspice_str2et( '01-Jan-2000 12:00:00' );
% OrbitalMech.ETStart   = cspice_str2et( OrbitalMech.DateStart );

[jd, jdfrac] = jday(...
    OrbitalMech.DateStart_vet(1),...
    OrbitalMech.DateStart_vet(2),...
    OrbitalMech.DateStart_vet(3),...
    OrbitalMech.DateStart_vet(4),...
    OrbitalMech.DateStart_vet(5),...
    OrbitalMech.DateStart_vet(6));

OrbitalMech.jd_frac = [jd, jdfrac];
OrbitalMech.jd = sum( OrbitalMech.jd_frac );


[mjd, mjdfrac] = jdayall(1858, 11, 17, 0, 0, 0, 'g');
OrbitalMech.jd2Mjd = sum( [ mjd , mjdfrac ] );
OrbitalMech.Mjd = OrbitalMech.jd - OrbitalMech.jd2Mjd;

[mjd_2000, mjdfrac_2000] = jdayall(2000,01,01,12,0,0,'g');
OrbitalMech.jdMjd2000 = sum( [ mjd_2000 , mjdfrac_2000 ] );
OrbitalMech.Mjd2000 = OrbitalMech.jd - OrbitalMech.jdMjd2000;

% OrbitalMech.ETStart = 0;
% OrbitalMech.DateStart = cspice_timout( OrbitalMech.ETStart , cspice_tpictr( '1-Oct-1111 11:11:11' ) );

%% Coordinate Time System
% Initialize UT1-UTC and TAI-UTC time difference
fid = fopen('eop19620101.txt','r');
%  ----------------------------------------------------------------------------------------------------
% |  Date    MJD      x         y       UT1-UTC      LOD       dPsi    dEpsilon     dX        dY    DAT
% |(0h UTC)           "         "          s          s          "        "          "         "     s 
%  ----------------------------------------------------------------------------------------------------
eopdata = fscanf(fid,'%i %d %d %i %f %f %f %f %f %f %f %f %i',[13 inf]);
fclose(fid);

OrbitalMech.eopdata = eopdata;


% [UT1_UTC,TAI_UTC,x_pole,y_pole,ddpsi,ddeps] = IERS(eopdata,OrbitalMech.Mjd,'l');
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,OrbitalMech.Mjd,'l');
[UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);

OrbitalMech.Mjd_TT = OrbitalMech.Mjd + TT_UTC/86400;

%%
%
OrbitalMech.CoorTime.timezone = 0;

OrbitalMech.CoorTime.dut1 = UT1_UTC;
OrbitalMech.CoorTime.dat = TAI_UTC;
OrbitalMech.CoorTime.lod = LOD;
OrbitalMech.CoorTime.xp = x_pole;
OrbitalMech.CoorTime.yp = y_pole;
OrbitalMech.CoorTime.eqeterms = 2;
OrbitalMech.CoorTime.terms = 2;
OrbitalMech.CoorTime.ddpsi = dpsi;
OrbitalMech.CoorTime.ddeps = deps;

%% Charateristicsof the Sun and Astrodynamic Constants
% [ OrbitalMech.Sun , OrbitalMech.AstroConst ] = AstroParameters( 'SUN' );

%% Charateristics of the Main Attractor.
% [ OrbitalMech.MainAttractor ] = OrbitalMech.Sun;
[ OrbitalMech.MainAttractor ] = AstroParameters( 'EARTH' );

%% List of names of other bodies that contribute to the dynamics.
% List = { ...
%      'SUN' , ...
%      'MERCURY' , ...
%      'VENUS' , ....
%      'EARTH' , 'MOON' , ...
%      'MARS' , 'PHOBOS' , 'DEIMOS' , ...
%      'JUPITER' , 'IO' , 'EUROPA' , 'GANYMEDE' , 'CALLISTO' , 'AMALTHEA' , 'HIMALIA' , 'HIMALIA' , 'PASIPHAE' , 'SINOPE' , 'LYSITHEA' , 'CARME' , ...
%                  'ANANKE' , 'LEDA' , 'THEBE' , 'ADRASTEA' , 'METIS' , ...
%      'SATURN' , 'MIMAS' , 'ENCELADUS' , 'TETHYS' , 'DIONE' , 'RHEA' , 'TITAN' , 'HYPERION' , 'IAPETUS' , 'PHOEBE' , 'JANUS' , 'EPIMETHEUS' , ...
%                 'HELENE' , 'TELESTO' , 'CALYPSO' , 'ATLAS' , 'PROMETHEUS' , 'PANDORA' , 'PAN' , ...
%      'URANUS' , 'ARIEL' , 'UMBRIEL' , 'TITANIA' , 'OBERON' , 'MIRANDA' , 'CORDELIA' , 'OPHELIA' , 'BIANCA', 'CRESSIDA' , 'DESDEMONA' , 'JULIET' , ...
%                 'JULIET' , 'PORTIA' , 'ROSALIND' , 'BELINDA' , 'PUCK' , ...
%      'NEPTUNE' , 'TRITON' , 'NEREID' , 'NAIAD' , 'THALASSA' , 'DESPINA' , 'GALATEA' , 'LARISSA' , 'PROTEUS' , ...
%      'PLUTO' , 'CHARON' };
% List = { 'SUN' };
% List = { 'SUN' , 'MOON' };
% List = { 'EARTH' };
% List = { 'SUN' , 'EARTH' , 'MOON' };
% List = { 'MOON' };
List = {  };

iTBody = 0;
for iBody = 1 : length( List )
    if strcmp( List{ iBody } , OrbitalMech.MainAttractor.Name ) == 0
        iTBody = iTBody + 1;
        [ Body ] = AstroParameters( List{ iBody } );
        OrbitalMech.CelestialBodies{ iTBody } = Body;
    end
end

%% Charateristics of the Eclipse Bodies.
% List of names of other bodies that can be eclipse the satellite.
% ListEclipB = { ...
%      'SUN' , ...
%      'MERCURY' , ...
%      'VENUS' , ....
%      'EARTH' , 'MOON' , ...
%      'MARS' , 'PHOBOS' , 'DEIMOS' , ...
%      'JUPITER' , 'IO' , 'EUROPA' , 'GANYMEDE' , 'CALLISTO' , 'AMALTHEA' , 'HIMALIA' , 'HIMALIA' , 'PASIPHAE' , 'SINOPE' , 'LYSITHEA' , 'CARME' , ...
%                  'ANANKE' , 'LEDA' , 'THEBE' , 'ADRASTEA' , 'METIS' , ...
%      'SATURN' , 'MIMAS' , 'ENCELADUS' , 'TETHYS' , 'DIONE' , 'RHEA' , 'TITAN' , 'HYPERION' , 'IAPETUS' , 'PHOEBE' , 'JANUS' , 'EPIMETHEUS' , ...
%                 'HELENE' , 'TELESTO' , 'CALYPSO' , 'ATLAS' , 'PROMETHEUS' , 'PANDORA' , 'PAN' , ...
%      'URANUS' , 'ARIEL' , 'UMBRIEL' , 'TITANIA' , 'OBERON' , 'MIRANDA' , 'CORDELIA' , 'OPHELIA' , 'BIANCA', 'CRESSIDA' , 'DESDEMONA' , 'JULIET' , ...
%                 'JULIET' , 'PORTIA' , 'ROSALIND' , 'BELINDA' , 'PUCK' , ...
%      'NEPTUNE' , 'TRITON' , 'NEREID' , 'NAIAD' , 'THALASSA' , 'DESPINA' , 'GALATEA' , 'LARISSA' , 'PROTEUS' , ...
%      'PLUTO' , 'CHARON' };
% ListEclipB = { 'EARTH' };
% ListEclipB = { 'EARTH' , 'MOON' };
% ListEclipB = { 'MOON' };
ListEclipB = {  };

iTBody = 0;
for iBody = 1 : length( ListEclipB )
    if strcmp( ListEclipB{ iBody } , OrbitalMech.Sun.Name ) == 0
        iTBody = iTBody + 1;
        [ Body ] = AstroParameters( ListEclipB{ iBody } );
        OrbitalMech.EclipsBodies{ iTBody } = Body;
    end
end

end