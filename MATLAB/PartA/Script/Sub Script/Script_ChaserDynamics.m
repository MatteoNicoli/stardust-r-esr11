%% Integrator setting.
t_Ma1 = 2 * ( 3600 );
t_Ma2 = 6 * ( 3600 );
Total_ToF = Data_los.t(end);%12 * ( 3600 );
% Total_ToF = 90*60;
tspan = [ 0 , Total_ToF ]';

%%
N = 4000;
tspan = linspace( tspan( 1 ) , tspan( end ) , N );
IC_State = [ 2.22031e+06 , 633762 , -6.81516e+06, 7015.79, -1138.41, 2170.52 ];
    
%%
t_ode = [  ];
X_ode = [  ];
t_ode_s = Data_los.t;
X_ode_s = [  ];
t_fin = tspan( 1 );
iter = 0;
while t_fin < Total_ToF
    iter = iter + 1;
    
    [ sol , i_t_ode , i_X_ode , te , xe , ie ] = ODEfnc( tspan , IC_State , Param );

    t_fin = i_t_ode( end );
    switch t_fin
        case { t_Ma1 }
            R = i_X_ode( end , 1 : 3 );
            V = i_X_ode( end , 4 : 6 );
            h = cross( R , V );
            h_norm = norm( h );
            v_Ma = 0.1.*1e-3 .* ( h ./ h_norm );
            
            IC_State = i_X_ode( end , : ) + [ 0 , 0 , 0 , v_Ma ];
            
        case { t_Ma2 }
            v_Ma = 0.1.*1e-3 .* ( - i_X_ode( end , 4 : 6 ) ./ norm( i_X_ode( end , 4 : 6 ) ) );
            
            IC_State = i_X_ode( end , : ) + [ 0 , 0 , 0 , v_Ma ];
            
        otherwise
    end
    
    [ ~ , idx( iter ) ]=max( Data_los.t > t_fin );
    switch iter
        case 1
            [ i_X_ode_s ] = deval( sol , t_ode_s( 1 : idx( iter ) - 1 ) );
            
        otherwise
            if t_fin == Total_ToF
                [ i_X_ode_s ] = deval( sol , t_ode_s( idx( iter - 1 ) : end ) );
                
            else
                [ i_X_ode_s ] = deval( sol , t_ode_s( idx( iter - 1 ) : idx( iter ) - 1 ) );
                
            end
    end
    X_ode_s = [ X_ode_s ; i_X_ode_s' ];
    
    
    tspan = linspace( t_fin , Total_ToF , N );
    if t_fin == Total_ToF
        t_ode = [ t_ode ; i_t_ode( 1 : end ) ];
        X_ode = [ X_ode ; i_X_ode( 1 : end , : ) ];
    else
        t_ode = [ t_ode ; i_t_ode( 1 : end - 1 ) ];
        X_ode = [ X_ode ; i_X_ode( 1 : end - 1 , : ) ];
    end
end

Rs = X_ode_s( : , 1 : 3 )';

%%
for it = 1 : length(t_ode)
    % Time
    jd1 = Param.OrbitalMech.jd_frac(1);
    jdfrac1 = Param.OrbitalMech.jd_frac(2)+( t_ode(it)/(24*3600) );
    
    [year,mon,day,hr,min,sec] = invjday ( jd1, jdfrac1 );
    [jd, jdfrac] = jdayall ( year,mon,day,hr,min,sec ,'g');
    [year,mon,day,hr,min,sec] = invjday ( jd, jdfrac );
    
    yeararr_c(it,1)=year;
    monarr_c(it,1)=mon;
    dayarr_c(it,1)=hr;
    hrarr_c(it,1)=year;
    minarr_c(it,1)=min;
    secarr_c(it,1)=sec;
    
    ChaserCar(it,1).jd = jd;
    ChaserCar(it,1).jdfrac = jdfrac;
    
    ChaserCar(it,1).year = year;
    ChaserCar(it,1).year = mon;
    ChaserCar(it,1).year = day;
    ChaserCar(it,1).year = hr;
    ChaserCar(it,1).year = min;
    ChaserCar(it,1).year = sec;
    
    %
%     dut1 = Param.OrbitalMech.CoorTime.dut1;
%     dat = Param.OrbitalMech.CoorTime.dat;
%     lod = Param.OrbitalMech.CoorTime.lod;
%     xp = Param.OrbitalMech.CoorTime.xp;
%     yp = Param.OrbitalMech.CoorTime.yp;
%     ddpsi = Param.OrbitalMech.CoorTime.ddpsi;
%     ddeps = Param.OrbitalMech.CoorTime.ddeps;
    
    terms = Param.OrbitalMech.CoorTime.terms;
    timezone = Param.OrbitalMech.CoorTime.timezone;
    Mjd_UTC = ( jd + jdfrac ) - Param.OrbitalMech.jd2Mjd;
    [xp,yp,dut1,lod,ddpsi,ddeps,dx_pole,dy_pole,dat] = IERS(eopdata,Mjd_UTC,'l');

    [ut1, tut1, jdut1, jdut1frac, utc, tai, tt, ttt1, jdtt, jdttfrac, tdb, ttdb, jdtdb, jdtdbfrac ] ...
          = convtime ( year,mon,day,hr,min,sec, timezone, dut1, dat );
    
    ChaserCar(it,1).ut1 = ut1;
    ChaserCar(it,1).tut1 = tut1;
    ChaserCar(it,1).jdut1 = jdut1;
    ChaserCar(it,1).jdut1frac = jdut1frac;
    ChaserCar(it,1).utc = utc;
    ChaserCar(it,1).tai = tai;
    ChaserCar(it,1).tt = tt;
    ChaserCar(it,1).ttt = ttt1;
    ChaserCar(it,1).jdtt = jdtt;
    ChaserCar(it,1).jdttfrac = jdttfrac;
    ChaserCar(it,1).tdb = tdb;
    ChaserCar(it,1).ttdb = ttdb;
    ChaserCar(it,1).jdtdb = jdtdb;
    ChaserCar(it,1).jdtdbfrac = jdtdbfrac;
    
    % Site eci to ecef
    r_eci = X_ode( it , 1 : 3 )';
    v_eci = X_ode( it , 4 : 6 )';
    
    [ ~ , decl_t , rtasc_t ] = car2sph( r_eci(1) , r_eci(2) , r_eci(3) );
    
    a_eci = [0;0;0];
    [r_ecef,v_ecef,a_ecef] = eci2ecef( r_eci,v_eci,a_eci,ttt1,jdut1+jdut1frac,lod,xp,yp,terms,ddpsi,ddeps );
    
    [lon, lat_gd, alt] = Geodetic(r_ecef,re,flat);
    
    ChaserCar(it,1).rs_eci = r_eci;
    ChaserCar(it,1).vs_eci = v_eci;
    ChaserCar(it,1).rs_ecef = r_ecef;
    ChaserCar(it,1).vs_ecef = v_ecef;
    ChaserCar(it,1).latgd = lat_gd;
    ChaserCar(it,1).lon = lon;
    ChaserCar(it,1).alt = alt;
    ChaserCar(it,1).rtasc = rtasc_t;
    ChaserCar(it,1).decl = decl_t;
    
end

%% ========================================================================
r_site_eci = X_ode_s( : , 1 : 3 )';
v_site_eci = X_ode_s( : , 4 : 6 )';

%%
for it = 1 : length(t_ode_s)
    %% Time
    jd1 = Param.OrbitalMech.jd_frac(1);
    jdfrac1 = Param.OrbitalMech.jd_frac(2)+Data_los.t(it)/(24*3600);
    
    [year,mon,day,hr,min,sec] = invjday ( jd1, jdfrac1 );
    [jd, jdfrac] = jdayall ( year,mon,day,hr,min,sec,'g' );
    [year,mon,day,hr,min,sec] = invjday ( jd, jdfrac );
    
    JD(it,1) = jd;
    JDfrac(it,1) = jdfrac;
    
    year_arr(it,1)=year;
    mon_arr(it,1)=mon;
    day_arr(it,1)=hr;
    hr_arr(it,1)=year;
    min_arr(it,1)=min;
    sec_arr(it,1)=sec;
    
    
    ObsCar(it,1).jd = jd;
    ObsCar(it,1).jdfrac = jdfrac;
    ObsCar(it,1).jdf = jdfrac;
    
    JD1 = jd + jdfrac;
    ObsCar(it,1).jd1 = JD1;
    ObsCar(it,1).time = jd;
    ObsCar(it,1).timef = jdfrac;
    
    ObsCar(it,1).year = year;
    ObsCar(it,1).mon = mon;
    ObsCar(it,1).day = day;
    ObsCar(it,1).hr = hr;
    ObsCar(it,1).min = min;
    ObsCar(it,1).sec = sec;
    
    %%
%     dut1 = Param.OrbitalMech.CoorTime.dut1;
%     dat = Param.OrbitalMech.CoorTime.dat;
%     lod = Param.OrbitalMech.CoorTime.lod;
%     xp = Param.OrbitalMech.CoorTime.xp;
%     yp = Param.OrbitalMech.CoorTime.yp;
%     ddpsi = Param.OrbitalMech.CoorTime.ddpsi;
%     ddeps = Param.OrbitalMech.CoorTime.ddeps;
    
    terms = Param.OrbitalMech.CoorTime.terms;
    timezone = Param.OrbitalMech.CoorTime.timezone;
    Mjd_UTC = ( jd + jdfrac ) - Param.OrbitalMech.jd2Mjd;
    [xp,yp,dut1,lod,ddpsi,ddeps,dx_pole,dy_pole,dat] = IERS(eopdata,Mjd_UTC,'l');
    
    [ut1, tut1, jdut1, jdut1frac, utc, tai, tt, ttt1, jdtt, jdttfrac, tdb, ttdb, jdtdb, jdtdbfrac ] ...
          = convtime ( year,mon,day,hr,min,sec,timezone, dut1, dat );
      
    jdut( it , 1 ) = jdut1;
    jdutfrac( it , 1 ) = jdut1frac;
    ttt( it , 1 ) = ttt1;
    
    ObsCar(it,1).ut1 = ut1;
    ObsCar(it,1).tut1 = tut1;
    ObsCar(it,1).jdut = jdut1;
    ObsCar(it,1).jdutfrac = jdut1frac;
    
    jdut_t = jdut1 + jdut1frac;
    ObsCar(it,1).jdut1 = jdut_t;
    ObsCar(it,1).jdut1frac = jdut1frac;
    
    ObsCar(it,1).utc = utc;
    ObsCar(it,1).tai = tai;
    ObsCar(it,1).tt = tt;
    ObsCar(it,1).ttt = ttt1;
    ObsCar(it,1).jdtt = jdtt;
    ObsCar(it,1).jdttfrac = jdttfrac;
    ObsCar(it,1).tdb = tdb;
    ObsCar(it,1).ttdb = ttdb;
    ObsCar(it,1).jdtdb = jdtdb;
    ObsCar(it,1).jdtdbfrac = jdtdbfrac;
    
    %% Site eci to ecef
    r_eci = r_site_eci( : , it );
    v_eci = v_site_eci( : , it );
    
    a_eci = [0;0;0];
    [r_ecef,v_ecef,a_ecef] = eci2ecef(r_eci,v_eci,a_eci,ttt1,jdut1 + jdut1frac,lod,xp,yp,terms,ddpsi,ddeps );
    
    [lon, lat_gd, alt] = Geodetic(r_ecef,re,flat);
    
    latarr(it,1) = lat_gd;
    lonarr(it,1) = lon;
    altarr(it,1) = alt;
    
    ObsCar(it,1).rs_eci = r_eci;
    ObsCar(it,1).vs_eci = v_eci;
    ObsCar(it,1).rs_ecef = r_ecef;
    ObsCar(it,1).vs_ecef = v_ecef;
    ObsCar(it,1).latgd = lat_gd;
    ObsCar(it,1).lon = lon;
    ObsCar(it,1).alt = alt;
%     ObsCar(it,1).rtasc_s = rtasc_s;
%     ObsCar(it,1).decl_s = decl_s;
    
    Site.lat(it,1) = lat_gd;
    Site.lon(it,1) = lon;
    Site.alt(it,1) = alt;
    Site.Rs(:,it) = r_ecef;
    
    site( it , : ) = [ lat_gd , lon , alt ];
    r_site_ecef( : , it ) = r_ecef;
    
    %% target
    [ ~ , i_decl_t , i_rtasc_t ] = car2sph( Data_los.r(it,1) , Data_los.r(it,2) , Data_los.r(it,3) );
%     [ rtasc , decl ] = cart2sph( Data_los.r(it,1) , Data_los.r(it,2) , Data_los.r(it,3) );
    
    ObsCar(it,1).rtasc = i_rtasc_t;
    ObsCar(it,1).decl = i_decl_t;
    
    Ra = (180/pi)*i_rtasc_t;
    Dec = (180/pi)*i_decl_t;
    lat = (180/pi)*lat_gd;
    lon = (180/pi)*lon;
    
    [ i_Az , i_El ] = RaDec2AzEl(Ra,Dec,lat,lon,JD1);
    
    obs_AzEl(it,1) = JD1-2400000.5;
    obs_AzEl(it,2) = (pi/180)*i_Az;
    obs_AzEl(it,3) = (pi/180)*i_El;
    obs_AzEl(it,3) = (pi/180)*i_El;
    
    obs_RaDec(it,1) = JD1-2400000.5;
    obs_RaDec(it,2) = i_rtasc_t;
    obs_RaDec(it,3) = i_decl_t;
    
end
