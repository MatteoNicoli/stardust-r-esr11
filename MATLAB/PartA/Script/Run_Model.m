%% Chaser Dynamics
Script_ChaserDynamics;

%% Initial Orbital Determination (Multiple Gauss Method)
iR = ([ObsCar.rs_eci].');
ijt = ([ObsCar.jd1].')*24*3600;

% 
r_taget = [  ];
v_taget = [  ];
r = [  ];
for i_slot = 1 : length( it_obs_slot ) - 1
    switch i_slot
        case length( it_obs_slot ) - 1
            R = iR( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) , : );
            rho_vet = Data_los.r( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) , : );
            t = ijt( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) );
            
        otherwise
            R = iR( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) - 1 , : );
            rho_vet = Data_los.r( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) - 1 , : );
            t = ijt( it_obs_slot( i_slot ) : it_obs_slot( i_slot + 1 ) - 1 );
            
    end
    
%     i_r_taget_1 = [  ];
%     i_r_taget_2 = [  ];
%     i_r_taget_3 = [  ];
%     for ii_t = 2 : ( length( t ) - 1 )
%         
%         R_i = R( ii_t - 1 : ii_t + 1 , : );
%         rho_vet_i =rho_vet( ii_t - 1 : ii_t + 1 , : );
%         t_i = t( ii_t - 1 : ii_t + 1 );
% 
%         [ i_r_taget , i_v_target , i_r ] = Nanglesg( R_i , rho_vet_i , t_i , mu );
%         
%         [ ~ , decl1 , rtasc1 ] = car2sph( rho_vet_i(1,1) , rho_vet_i(1,2) , rho_vet_i(1,3) );
%         [ ~ , decl2 , rtasc2 ] = car2sph( rho_vet_i(2,1) , rho_vet_i(2,2) , rho_vet_i(2,3) );
%         [ ~ , decl3 , rtasc3 ] = car2sph( rho_vet_i(3,1) , rho_vet_i(3,2) , rho_vet_i(3,3) );
%         t1 = t_i( 1 ) - t_i( 2 );
%         t3 = t_i( 3 ) - t_i( 2 );
%         Rs1 = R_i( 1 , : )';
%         Rs2 = R_i( 2 , : )';
%         Rs3 = R_i( 3 , : )';
% % %         [i_r_taget,i_v_target] = anglesdr_RADec(...
% % %                                         rtasc1,rtasc2,rtasc3,...
% % %                                         decl1,decl2,decl3,...
% % %                                         t1,t3,...
% % %                                         Rs1,Rs2,Rs3,...
% % %                                         R_Earth,GM_Earth);
% %         [r2_g,v2_g] = anglesg_RADec(rtasc1,rtasc2,rtasc3,...
% %                                     decl1,decl2,decl3,...
% %                                     t1,t3,...
% %                                     Rs1,Rs2,Rs3,...
% %                                     GM_Earth);
% 
% 
%         i_r_taget_1( ii_t - 1 , : ) = i_r_taget( 1 , : );
%         i_r_taget_2( ii_t - 1 , : ) = i_r_taget( 2 , : );
%         i_r_taget_3( ii_t - 1 , : ) = i_r_taget( 3 , : );
% 
%         i_v_taget_2( ii_t - 1 , : ) = i_v_target( 1 , : );
% 
% %         i_r_1( ii_t - 1 , 1 ) = i_r( 1 );
% %         i_r_2( ii_t - 1 , 1 ) = i_r( 2 );
% %         i_r_3( ii_t - 1 , 1 ) = i_r( 3 );
% 
%     end
%     ii_v_taget = [ i_v_taget_2 ];
%     ii_r_taget = [ i_r_taget_1( 1 , : ) ; i_r_taget_2 ; i_r_taget_3( end , : ) ];
%     
    [ ii_r_taget , ii_v_taget , ii_r ] = Nanglesg( R , rho_vet , t , mu );
    r_taget = [ r_taget ; ii_r_taget ];
    v_taget = [ v_taget ; ii_v_taget ];
end

dr = sqrt( sum( ( r_taget - iR ).^2 , 2 ) );

%%
figure
hold on; grid on;

plot( t_ode_s ./ 3600 , dr , 'Linewidth' , SA );

xlabel( 't [h]' );
ylabel( '\Delta r [m]' );

%% 
sigma_range = 92.5;     % [m]
sigma_az = 0.0224*(pi/180);  % [rad]
sigma_el = 0.0139*(pi/180);  % [rad]

sigma.range = sigma_range;
sigma.az = sigma_az;
sigma.el = sigma_el;

r2_dr = r_taget( 2 , : )';
v2_dr = v_taget( 1 , : )';
obs_2 = obs_RaDec( 1 : 8 , : );
obs = obs_AzEl( 1 : 8 , : );
obs(1:8,4)=dr(1:8);

%%
[r1_sb,v1_sb,r1,v1] = OrbitDet_DiffCorLSQ(r_taget(1:8,:),JD,JDfrac,jdut,jdutfrac,ttt,r_site_ecef,site,sigma,2,Param);
rdif_sb = (X_ode_s(1,:)-[r1_sb,v1_sb])';
rdif_1 = (X_ode_s(1,:)-[r1,v1])';

fprintf(1,'\nSequential-Batch Differential Corrector of the Least Squares \n' );
fprintf(1,'r %16.8f %16.8f %16.8f m \n',rdif_sb(1:3,:) );
fprintf(1,'v %16.8f %16.8f %16.8f m/s \n',rdif_sb(4:6,:) );

fprintf(1,'\nDifferential Corrector of the Least Squares \n' );
fprintf(1,'r %16.8f %16.8f %16.8f m \n',rdif_1(1:3,:) );
fprintf(1,'v %16.8f %16.8f %16.8f m/s \n',rdif_1(4:6,:) )

% norm(rdif_sb(1:3))*1e-3
% norm(rdif_1(1:3))*1e-3

%%
[ Y0 ] = ExtendedKalmanFilter( r2_dr , v2_dr , obs , Site , sigma , Param );
Y0_EKF = (X_ode_s(1,:)-Y0')';

% norm(Y0_EKF(1:3))*1e-3

fprintf(1,'\nExtende Kalman Filter \n' );
fprintf(1,'r %16.8f %16.8f %16.8f m \n',Y0_EKF(1:3,:) );
fprintf(1,'v %16.8f %16.8f %16.8f m/s \n',Y0_EKF(4:6,:) )