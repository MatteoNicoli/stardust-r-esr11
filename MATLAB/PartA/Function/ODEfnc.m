function [ sol , t , x , te , xe , ie ] = ODEfnc( tspan , IC , Param , varargin )
%% Date

%% Integrator
odeopt = odeset( ...
    'AbsTol'     , 2.22045e-14  ,...
    'RelTol'     , 2.22045e-14  ,...
    'Events'     , @myEventsfcn );

% odeopt = odeset( ...
%     'AbsTol'     , 2.22045e-14  ,...
%     'RelTol'     , 2.22045e-14  ,...
%     'OutputFcn'  , @myOutputfcn ,...
%     'Events'     , @myEventsfcn ); %, ...
%     'Jacobian'   , @Jacfcn ); %,...
%     'Mass'       , @Massfcn ); %,...
%     'Vectorized' , 'on' , ...
%     );

%%
global AuxParam eopdata

GM = Param.OrbitalMech.MainAttractor.GM;
re = Param.OrbitalMech.MainAttractor.R;

param = [ GM , re ];

eopdata = Param.OrbitalMech.eopdata;

AuxParam.Mjd_TT = Param.OrbitalMech.Mjd_TT;
AuxParam.n      = 10;
AuxParam.m      = 10;
AuxParam.n_a    = 10;
AuxParam.m_a    = 10;
AuxParam.n_G    = 10;
AuxParam.m_G    = 10;

%% O.D.E. Numerical Integration
% [ sol ] = ode113( @odefnc , tspan , IC , odeopt );
[ sol ] = ode113( @Accel , tspan , IC , odeopt , param );

t = sol.x';
x = sol.y';
te = sol.xe';
xe = sol.ye';
ie = sol.ie';

%% Function
    %----------------------------------------------------------------------
    % odefcn
    %----------------------------------------------------------------------
    function [ dydt ] = odefnc( t , x , varargin )
        
        [ dydt ] = NewtonEqua( t , x , Param );
        
    end

    %----------------------------------------------------------------------
    % Jacfcn
    %----------------------------------------------------------------------
%     function [ Jac ] = Jacfcn( t , x , varargin )
%         
%         Jac = Param.afun.odefun.Jac( t , x );
%         
%     end

    %----------------------------------------------------------------------
    % Massfcn
    %----------------------------------------------------------------------
%     function [ M ] = Massfcn( t , x , varargin )
%         
%         M = Param.afun.odefun.M( t , x );
%         
%     end

    %----------------------------------------------------------------------
    % myEventsfcn
    %----------------------------------------------------------------------
    function [ value , isterminal , direction ] = myEventsfcn ( t , x , varargin )
        
        value = [ t - 2 * ( 3600 ) ;
                  t - 6 * ( 3600 ) ];
        
        isterminal = [ 1 ;
                       1 ];
        
        direction = [ 0 ;
                      0 ];
        
%         value = [ t - 2 * ( 3600 ) ;
%                   t - 6 * ( 3600 ) ];
%         
%         isterminal = [ 0 ;
%                        0 ];
%         
%         direction = [ 0 ;
%                       0 ];
        
    end

    %----------------------------------------------------------------------
    % myOutputfcn
    %----------------------------------------------------------------------
%     function Status = myOutputfcn ( t , x , flag , varargin )
%         
%         if isempty( flag )
%             
%         else
%             
%             switch ( flag )
%                 
%                 case { 'init' }
%                     
%                 case { 'done' }
%                     
% %                     A = [ A , t ];
%                     
%                 otherwise
% 
% %                     A = [ A , t ];
%                     
%             end
%             
%         end
%         
%         Status = 0;
%         
%     end

end