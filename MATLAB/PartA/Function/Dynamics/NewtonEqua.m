function [ X_d ] = NewtonEqua( t , X , Param )

r_N = X( 1 : 3 );

%%
[ a_N ] = EphRnBPmodel( t , r_N , Param );

%%
% [ r , phi , lambda ] = car2sph( r_N( 1 ) , r_N( 2 ) , r_N( 3 ) );
% GM = Param.OrbitalMech.MainAttractor.GM;
% R = Param.OrbitalMech.MainAttractor.R;
% C_nm = Param.OrbitalMech.MainAttractor.C_nm;
% S_nm = Param.OrbitalMech.MainAttractor.S_nm;

% [ aI_GF ] = GravityField_accI_25( r_N , phi , lambda , R , GM , C_nm , S_nm );
% [ aJ_GF ] = GravityField_accJ_25( r_N , phi , lambda , R , GM , C_nm , S_nm );
% [ aK_GF ] = GravityField_accK_25( r_N , phi , lambda , R , GM , C_nm , S_nm );
% 
% a_ECEF = [ aI_GF , aJ_GF , aK_GF ];
% 
% r_ECI = r_N;
% v_ECI = X( 4 : 6 );
% [r_ECEF ] = r_ECItoECEF(JD,r_ECI);
% [v_ECEF ] = v_ECItoECEF(JD,r_ECI,v_ECI);
% [a_ECI] = a_ECEFtoECI(JD,r_ECEF,v_ECEF,a_ECEF);
a_ECI = [ 0 ; 0 ; 0 ];

%%
a_N = a_N + a_ECI;

X_d = [ X( 4 : 6 ) ; a_N ];

end