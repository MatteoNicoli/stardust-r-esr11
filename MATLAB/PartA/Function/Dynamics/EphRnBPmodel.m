%{
%-Abstract
%
%   EPHRNBPMODEL: This function performs the calculation of the Newton 
%                 equation of the restricted n� bodies problem.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      t:       Time istant of the simulation, (1,n): [s].
%      r_N:     Position vector, (3,n): [m].
%      Param:   Structure contain the charateristics of the Sun, main 
%               atractor, reference frame, abberation correction, ET start.
%
%   The call:
%      
%      [ a_N ] = EphRnBPmodel( t , r_N , Param );
%
%   Returns:
%
%      a_N: Acceeration vector, (3,n): [m/s^2].
%
%-Version
%
%   -Version 3.0.0, 02-FEB-2017.
%
%-&
%}

function [ a_N ] = EphRnBPmodel( t , r_N , Param )
%% Inizialization
OrbitalMech = Param.OrbitalMech;

%% MajorAtractor-S/C
% Compute the acceleration due to the main attractor.
a_N = - OrbitalMech.MainAttractor.GM * r_N ./ sqrt( sum( r_N.^2 , 1 ) ).^3;     % Acceleration vector, (3,n), due to the main attractor [m/s^2].

% Other Bodies Influence
% Calculation of the gravitational contribution of the previous bodies listed.
if isfield( OrbitalMech , 'CelestialBodies' ) > 0
    ET = OrbitalMech.ETStart + t';	% Compute the Ephemerides time (J2000), (1,n), for each time istant [s].
    for iBody = 1 : length( OrbitalMech.CelestialBodies )
%         GM = cspice_bodvrd( OrbitalMech.CelestialBodies( iBody ).Name , 'GM' , 1 ) * 1e9;	% Standard gravitational parameter for the i-th body [m^3/s^2].
        GM = OrbitalMech.CelestialBodies{ iBody }.GM;                           % Standard gravitational parameter for the i-th body [m^3/s^2].
        % Compute state vector, (6,n), of the i-th body that contribute to
        % the dynamics [m,m/s].
        [ State ] = cspice_spkezr( OrbitalMech.CelestialBodies{ iBody }.Name , ...
                                   ET                                        , ...
                                   OrbitalMech.RefCorr.RefFrame              , ...
                                   OrbitalMech.RefCorr.AbCorr                , ...
                                   OrbitalMech.MainAttractor.Name            );
%         [ State ] = uplanetECI( ET , OrbitalMech.CelestialBodies{ iBody } , GM );
        
        State = State;
        ir_N = r_N - State( 1 : 3 , : );                                         % Position vector, (3,n), of the satellite w.r.t. the i-th body [m];

        a_N_B = GM * ( - ir_N ./ sqrt( sum( ir_N.^2 , 1 ) ).^3 );                 % Acceleration vector, (3,n), due to the i-th body [m/s^2].
        a_N = a_N + a_N_B;
    end
end

end