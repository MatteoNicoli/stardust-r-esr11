function [x,F,niter] = newtonsys (Ffun ,Jfun ,x0 ,tol ,nmax , varargin )
%NEWTONSYS calcola una radice di un sistema non lineare
% [ZERO ,F,NITER]= NEWTONSYS (FFUN ,JFUN ,X0 ,TOL ,NMAX )
% calcola il vettore ZERO , radice di un sistema non
% lineare definito nella function FFUN con matrice
% jacobiana definita nella function JFUN a partire
% dal vettore X0. F contiene il valore del residuo
% in ZERO e NITER il numero di iterazioni necessarie
% per calcolare ZERO . FFUN e JFUN sono function
% definite tramite M-file

niter = 0;
err = tol + 1;
x = x0;

while err >= tol & niter < nmax
    J = feval(Jfun ,x,varargin {:});
    F = feval(Ffun ,x,varargin {:});
    delta = - J\F;
    x = x + delta;
    err = norm (delta);
    niter = niter + 1;
end

F = norm (feval(Ffun ,x,varargin {:}));

% if (niter== nmax & err > tol)
%     fprintf (['The method does not converge to the maximum ' ,'number of iterations.\nThe last iterated computed has relative residual equal to %e\n'],F);
% else
%     fprintf (['The method converts  in %i iterations ' ,' with a residue equal to %e\n'],niter ,F);
% end

return