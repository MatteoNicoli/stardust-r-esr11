%{
%-Abstract
%
%   N2B: This function performs the trasfromation cordinates from inerzial
%        to body fixed frame.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      A_BN:       Direct cosine matrix, (3,3), from inerzial to body fixed
%                  frame [-].
%      vect_N:     Vector, (3,1), in inerzial frame [-].
%
%   The call:
%      
%      [ vect_B ] = N2B( A_BN , vect_N );
%
%   Returns:
%
%      vect_B: Vector, (3,1), in body fixed frame [-].
%
%-Version
%
%   -Version 1.0.0, 07-NOV-2017.
%
%-&
%}

function [ vect_B ] = N2B( A_BN , vect_N )

A_BN   = reshape( A_BN , [ 3 , 3 ] ); % Direct cosine matrix, (3,3), from inerzial to body fixed frame [-].
vect_B = A_BN * vect_N;               % Vector, (3,1), in body fixed frame [-].

end