%{
%-Abstract
%
%   CAR2SPH: Transform Cartesian to spherical coordinates.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      x:	
%      y:	
%      z:	
%
%   The call:
%      
%      [ r , azimuth , elevation ] = car2sph( x , y , z )
%
%   Returns:
%
%      r:           
%      elevation:   
%      azimuth:     
%
%-Version
%
%   -Version 1.0.0, 12-MAY-2019.
%
%-&
%}

function [ r , elevation , azimuth ] = car2sph( x , y , z )

azimuth = atan2(y,x);
elevation = atan2(z,sqrt(x.^2 + y.^2));
r = sqrt(x.^2 + y.^2 + z.^2);

end