%{
%-Abstract
%
%   B2N: This function performs the trasfromation cordinates from body 
%        fixed to inerzial frame.
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY NICOLI MATTEO.
%
%-I/O
%
%   Given:
%      
%      A_BN:       Direct cosine matrix, (3,3), from inerzial to body fixed
%                  frame [-].
%      vect_B:     Vector, (3,1), in body fixed frame [-].
%
%   The call:
%      
%      [ vect_N ] = N2B( A_BN , vect_B );
%
%   Returns:
%
%      vect_N: Vector, (3,1), in inerzial frame [-].
%
%-Version
%
%   -Version 1.0.0, 07-NOV-2017.
%
%-&
%}

function [ vect_N ] = B2N( A_BN , vect_B )

A_BN   = reshape( A_BN , [ 3 , 3 ] );  % Direct cosine matrix, (3,3), from inerzial to body fixed frame [-].
vect_N = A_BN' * vect_B;               % Vector, (3,1), in inerzial frame [-].

end