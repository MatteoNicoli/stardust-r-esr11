%{
%-&========================================================================
%-Abstract
%
%   SETHELP: this function sets help flags to control intermediate output 
%            during debugging
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    none
%
%   The call:
%      
%      sethelp;
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    iauhelp;
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

iauhelp = 'n';

iaupnhelp = 'y';

show = 'n';

