%{
%-&========================================================================
%-Abstract
%
%   COSTMATH: this function sets constants for mathematical operations. 
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    none
%
%   The call:
%      
%     constmath;
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    rad, twopi, halfpi;
%    ft2m, mile2m, nm2m, mile2ft, mileph2kmph, nmph2kmph;
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

small = 1.0e-10;

infinite  = 999999.9;
undefined = 999999.1;

% -------------------------  mathematical  --------------------
rad    = 180.0 / pi;
twopi  = 2.0 * pi;
halfpi = pi * 0.5;

% -------------------------  conversions  ---------------------
ft2m    =    0.3048;
mile2m  = 1609.344;
nm2m    = 1852;
mile2ft = 5280;
mileph2kmph = 0.44704;
nmph2kmph   = 0.5144444;

  