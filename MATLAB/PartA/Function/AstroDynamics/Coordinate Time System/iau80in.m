%{
%-&========================================================================
%-Abstract
%
%   IAU80IN: this function initializes the nutation matricies needed for 
%            reduction calculations. the routine needs the filename of the 
%            files as input.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    iar80       - integers for fk5 1980
%    rar80       - reals for fk5 1980             rad
%
%   The call:
%      
%      [iar80,rar80] = iau80in();
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    none
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [iar80,rar80] = iau80in();

% ------------------------  implementation   -------------------
% 0.0001" to rad
convrt= 0.0001 * pi / (180*3600.0);

load nut80.dat;

iar80 = nut80(:,1:5);
rar80 = nut80(:,6:9);

for i=1:106
   for j=1:4
       rar80(i,j)= rar80(i,j) * convrt;
   end
end

end
