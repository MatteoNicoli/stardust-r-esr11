%{
%-&========================================================================
%-Abstract
%
%   GSTIME: this function finds the greenwich sidereal time (iau-82).
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    jdut1       - julian date of ut1             days from 4713 bc
%
%   The call:
%      
%      gst = gstime(jdut1);
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    gst         - greenwich sidereal time        0 to 2pi rad
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function gst = gstime(jdut1);

twopi      = 2.0*pi;
deg2rad    = pi/180.0;

% ------------------------  implementation   ------------------
tut1= ( jdut1 - 2451545.0 ) / 36525.0;

temp = - 6.2e-6 * tut1 * tut1 * tut1 + 0.093104 * tut1 * tut1  ...
       + (876600.0 * 3600.0 + 8640184.812866) * tut1 + 67310.54841;

% 360/86400 = 1/240, to deg, to rad
temp = rem( temp*deg2rad/240.0,twopi );

% ------------------------ check quadrants --------------------
if ( temp < 0.0 )
    temp = temp + twopi;
end

gst = temp;

end

