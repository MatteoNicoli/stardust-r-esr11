%{
%-&========================================================================
%-Abstract
%
%   JD2SSE: this function finds the seconds since epoch (1 Jan 2000) given 
%           the julian date
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    jd          - julian date                    days from 4713 bc
%
%   The call:
%      
%      sse = jd2sse( jd );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    sse         - seconds since epoch 1 jan 2000
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function sse = jd2sse( jd );

% ------------------------  implementation   ------------------
sse = (jd - 2451544.5) * 86400.0;

end
