%{
%-&========================================================================
%-Abstract
%
%   JDAYALL: this function finds the julian date given the year, month, 
%            day, and time. The julian date is defined by each elapsed day 
%            since noon, jan 1, 4713 bc.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    year        - year                           1900 .. 2100
%    mon         - month                          1 .. 12
%    day         - day                            1 .. 28,29,30,31
%    hr          - universal time hour            0 .. 23
%    min         - universal time min             0 .. 59
%    sec         - universal time sec             0.0 .. 59.999
%    whichtype   - julian or gregorian calender   'j' or 'g'
%
%   The call:
%      
%      [jd,jdfrac] = jdayall(year, mon, day, hr, min, sec, whichtype);
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    jd          - julian date                    days from 4713 bc
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [jd,jdfrac] = jdayall(year, mon, day, hr, min, sec, whichtype);

if mon <= 2
    year= year - 1;
    mon = mon + 12;
end

if whichtype == 'j'
    % --------- use for julian calender, every 4 years --------
    b= 0.0;
else
    % ---------------------- use for gregorian ----------------
    b= 2 - floor(year*0.01) + floor(floor(year*0.01)*0.25);
end

jd= floor( 365.25*(year + 4716) ) ...
     + floor( 30.6001*(mon+1) ) ...
     + day + b - 1524.5;
jdfrac = (sec + min * 60.0 + hr *3600.0) / 86400.0;

% check jdfrac
if jdfrac > 1.0 
    jd = jd + floor(jdfrac);
    jdfrac = jdfrac - floor(jdfrac);
end

end