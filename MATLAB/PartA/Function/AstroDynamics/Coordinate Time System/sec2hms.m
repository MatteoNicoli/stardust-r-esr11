%{
%-&========================================================================
%-Abstract
%
%   SEC2HMS: this function converts seconds from the beginning of the day 
%            into hours, minutes and seconds.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    utsec       - seconds                        0.0 .. 86400.0
%
%   The call:
%      
%      [hr,min,sec] = sec2hms( utsec );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    hr          - hours                          0 .. 24
%    min         - minutes                        0 .. 59
%    sec         - seconds                        0.0 .. 59.99
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [hr,min,sec] = sec2hms( utsec );

% ------------------------  implementation   ------------------
temp  = utsec / 3600.0;
hr    = fix( temp );
min   = fix( (temp - hr)* 60.0 );
sec   = (temp - hr - min/60.0 ) * 3600.0;

end