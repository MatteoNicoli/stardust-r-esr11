%{
%-&========================================================================
%-Abstract
%
%   HMS2SEC: this function converts hours, minutes and seconds into seconds
%            from the beginning of the day.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    hr          - hours                          0 .. 24
%    min         - minutes                        0 .. 59
%    sec         - seconds                        0.0 .. 59.99
%
%   The call:
%      
%      [utsec ] = hms2sec( hr,min,sec );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    utsec       - seconds                        0.0 .. 86400.0
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [utsec ] = hms2sec( hr,min,sec );

% ------------------------  implementation   ------------------
utsec  = hr * 3600.0 + min * 60.0 + sec;

end

