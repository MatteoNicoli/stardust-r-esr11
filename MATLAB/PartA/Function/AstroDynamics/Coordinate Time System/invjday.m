%{
%-&========================================================================
%-Abstract
%
%   INVJDAY: this function finds the year, month, day, hour, minute and 
%            second given the julian date. tu can be ut1, tdt, tdb, etc.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    jd          - julian date                    days from 4713 bc
%
%   The call:
%      
%      [year,mon,day,hr,min,sec] = invjday ( jd, jdfrac );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    year        - year                           1900 .. 2100
%    mon         - month                          1 .. 12
%    day         - day                            1 .. 28,29,30,31
%    hr          - hour                           0 .. 23
%    min         - minute                         0 .. 59
%    sec         - second                         0.0 .. 59.999
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [year,mon,day,hr,min,sec] = invjday ( jd, jdfrac );

% check jdfrac for multiple days
if abs(jdfrac) >= 1.0 
    jd = jd + floor(jdfrac);
    jdfrac = jdfrac - floor(jdfrac);
end

% check for fraction of a day included in the jd
dt = jd - floor(jd) - 0.5;
if (abs(dt) > 0.00000001)
    jd = jd - dt;  
    jdfrac = jdfrac + dt;
end

% ----------------- find year and days of the year ---------------
temp   = jd - 2415019.5; 
tu     = temp / 365.25;
year   = 1900 + floor( tu );
leapyrs= floor( ( year-1901 )*0.25 );
days   = floor(temp - ((year-1900)*365.0 + leapyrs ));

% ------------ check for case of beginning of a year -------------
if days + jdfrac < 1.0
    year   = year - 1;
    leapyrs= floor( ( year-1901 )*0.25 );
    days   = floor(temp - ((year-1900)*365.0 + leapyrs ));
end

% ------------------- find remaining data  -----------------------
% now add the daily time in to preserve accuracy
[mon,day,hr,min,sec] = days2mdh( year, days + jdfrac );
     
end