%{
%-&========================================================================
%-Abstract
%
%   LSTTIME: this function finds the local sidereal time at a given 
%            location.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    lon         - site longitude (west -)        -2pi to 2pi rad
%    jd          - julian date                    days from 4713 bc
%
%   The call:
%      
%      [lst,gst] = lstime ( lon, jd );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    lst         - local sidereal time            0.0 to 2pi rad
%    gst         - greenwich sidereal time        0.0 to 2pi rad
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [lst,gst] = lstime ( lon, jd );

twopi  = 2.0*pi;

% ------------------------  implementation   ------------------
[gst] = gstime( jd );
lst = lon + gst;

% ----------------------- check quadrants ---------------------
lst = rem( lst,twopi );
if ( lst < 0.0 )
    lst= lst + twopi;
end

end

