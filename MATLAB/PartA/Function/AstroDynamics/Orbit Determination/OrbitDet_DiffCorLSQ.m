%{
%-&========================================================================
%-Abstract
%
%   OrbitDet_DiffCorLSQ: Differential Corrector of the Least Squares  and
%                        Sequential-Batch Differential Corrector of the 
%                        Least Squares for orbital determination
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    r_obs          observation postion vector
%    jd             julian date
%    jdfrac         fraction of julian date
%    jdut           corrected julian date
%    jdutfrac       corrected fraction of julian date
%    ttt            julian centuries
%    rs_ecef        site position vector
%    site           site coordinate [geodetic latitude, longitude, altitude]
%    sigma          error of the observation
%    obstype        type of observation
%    Param          structure of constant parameter 
%
%   The call:
%      
%      [r1_sb,v1_sb,r1,v1] = OrbitDet_DiffCorLSQ(r_obs,jd,jdfrac,jdut,jdutfrac,ttt,rs_ecef,site,sigma,obstype,Param)
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    r1_sb           initial position vector Sequential-Batch Differential Corrector of the Least Squares
%    v1_sb           initial velocity vector Sequential-Batch Differential Corrector of the Least Squares   
%    r1              initial position vector Differential Corrector of the Least Squares
%    v1              initial velocity vector Differential Corrector of the Least Squares
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [r1_sb,v1_sb,r1,v1] = OrbitDet_DiffCorLSQ(r_obs,jd,jdfrac,jdut,jdutfrac,ttt,rs_ecef,site,sigma,obstype,Param)
%%
global eopdata

mu = Param.OrbitalMech.MainAttractor.GM;
GM = mu;
re = Param.OrbitalMech.MainAttractor.R;
param = [ GM , re ];

eopdata = Param.OrbitalMech.eopdata;

xp = Param.OrbitalMech.CoorTime.xp;
yp = Param.OrbitalMech.CoorTime.yp;
lod = Param.OrbitalMech.CoorTime.lod;
ddpsi = Param.OrbitalMech.CoorTime.ddpsi; 
ddeps = Param.OrbitalMech.CoorTime.ddeps;

terms = Param.OrbitalMech.CoorTime.terms;

%%
numobs = size( r_obs , 1 );

for j = 1:numobs  % 5 iterations for now
    % Time
    jd1 = jd(j,1) + jdfrac(j,1);
    jdut1 = jdut(j,1)+jdutfrac(j,1);
    i_ttt = ttt(j,1);
    
    % Site
    i_rs_ecef = rs_ecef(1:3,j);
    lat_gd = site(j,1);
    lon = site(j,2);
    
    %
    reci = r_obs( j , : )';
    veci = [0;0;0];   % dummy velocity variable for the ecef2eci routine
    
    Mjd_UTC = jd1 - Param.OrbitalMech.jd2Mjd;
    [xp,yp,dut1,lod,ddpsi,ddeps,dx_pole,dy_pole,dat] = IERS(eopdata,Mjd_UTC,'l');
    
    [rho,az,el,drho,daz,del] = rv2razel_m ( reci,veci, i_rs_ecef,lat_gd,lon,i_ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps );
    
    obsrecarr(j,1).rng = rho - 0*1e3;
    obsrecarr(j,1).az = az - 0*(pi/180);
    obsrecarr(j,1).el = el - 0*(pi/180);
end

%%
firstobs = 1;
lastobs = 8;

odeopt = odeset( 'AbsTol'     , 2.22045e-14  ,...
                 'RelTol'     , 2.22045e-14  );
     
% -------  form nominal vector
reci = [0 0 0];
veci = [0 0 0];
for obsktr = firstobs + 1 : lastobs - 1
    re1 = r_obs( obsktr - 1 , : )';
    re2 = r_obs( obsktr , : )';
    re3 = r_obs( obsktr + 1 , : )';
    
    jdut1_1 = jdut(obsktr-1)+jdutfrac(obsktr-1);
    jdut1_2 = jdut(obsktr)+jdutfrac(obsktr);
    jdut1_3 = jdut(obsktr+1)+jdutfrac(obsktr+1);
    
    dt21 = (jdut1_2-jdut1_1)*(24*3600);
    dt32 = (jdut1_3-jdut1_2)*(24*3600);
    
    [ve2, theta,theta1,copa, error] = hgibbs_m( re1,re2,re3,dt21,dt32 ,mu);

    % move back to 1st time, not central time
    dtsec = (jd(obsktr) + jdfrac(obsktr) - jd(1) - jdfrac(1)) * 86400.0;  % s
    
%     [reci1, veci1] =  kepler ( re2, ve2, -dtsec ,mu);
%     [reci1, veci1] =  pkeplerh ( re2, ve2, -dtsec, 0.0, 0.0 ,re,mu);
%     [~,X]=ode113(@NewtonEqua,[0,-dtsec],[re2', ve2'],odeopt,Param);
    [ sol ] = ode113( @Accel , [0,-dtsec] , [re2', ve2'] , odeopt , param );
    X = sol.y';
%     [t,X]=ode113(@ode_kepler,[0,-dtsec],[re2', ve2'],odeopt,mu);
        
    reci1 = X(end,1:3);
    veci1 = X(end,4:6);
    
    reci = reci + reci1;
    veci = veci + veci1;
end

%% --------------------------------------- now do differential correction part  -----------------------------------
reci = reci/(lastobs-firstobs-1);
veci = veci/(lastobs-firstobs-1);

xnom(1,1) = reci(1);
xnom(2,1) = reci(2);
xnom(3,1) = reci(3);
xnom(4,1) = veci(1);
xnom(5,1) = veci(2);
xnom(6,1) = veci(3);

NumWork = (lastobs-firstobs);
percentchg = 0.010;
deltaamtchg = 0.01;
% percentchg = 0.001;
% deltaamtchg = 0.0000001;
    
for j = 1:5  % 5 iterations for now

    % ---- accumulate obs and assemble matrices
    [atwa, atwb, atw, b, drng2, daz2, del2] = findatwaatwb_m(firstobs, lastobs,sigma,obstype,jd,jdfrac,jdut,jdutfrac,ttt,rs_ecef,site, obsrecarr, 6, percentchg, deltaamtchg, xnom,Param);

    deltax = inv(atwa)*atwb;
%     norm(deltax)
    xnom = xnom + deltax;
    r1(1) = xnom(1,1);
    r1(2) = xnom(2,1);
    r1(3) = xnom(3,1);
    v1(1) = xnom(4,1);
    v1(2) = xnom(5,1);
    v1(3) = xnom(6,1);

    switch (obstype)
        case 0
            sigma_new= sqrt( drng2 / NumWork );
        case 1
            sigma_new= sqrt( (daz2 + del2) / NumWork );
        case 2
            sigma_new = sqrt( (drng2 + daz2 + del2) / NumWork );
        case 3
            sigma_new= sqrt( (DRng2 + DAz2 + DEl2 + DDRng2) / NumWork );
        case 4
            sigma_new= sqrt( (DRng2 + DAz2 + DEl2 + DDRng2 + DDAz2 + DDEl2) / NumWork );
        case 5
            sigma_new= sqrt( (DTRtAsc2 + DTDecl2) / NumWork );
        case 7
            sigma_new= sqrt( DRng2 / NumWork );
    end % Case 

end % through iterations    

%% --------------------------------------- now do sequential part  -----------------------------------
lastobs = 8;
atwa_old = atwa;
atwb_old = atwb;

xnom(1,1) = r1(1);
xnom(2,1) = r1(2);
xnom(3,1) = r1(3);
xnom(4,1) = v1(1);
xnom(5,1) = v1(2);
xnom(6,1) = v1(3);

for j = 1:3  % 3 iterations for sequential batch
    
    % ---- accumulate obs and assemble matrices
    [atwa, atwb, atw, b, drng2, daz2, del2] = findatwaatwb_m(firstobs, lastobs,sigma,obstype,jd,jdfrac,jdut,jdutfrac,ttt,rs_ecef,site, obsrecarr, 6, percentchg, deltaamtchg, xnom,Param);

    deltax = inv(atwa + atwa_old)*(atwb + atwb_old);
%     norm(deltax)
    xnom = xnom + deltax;
    r1_sb(1) = xnom(1,1);
    r1_sb(2) = xnom(2,1);
    r1_sb(3) = xnom(3,1);
    v1_sb(1) = xnom(4,1);
    v1_sb(2) = xnom(5,1);
    v1_sb(3) = xnom(6,1);
    
    switch (obstype)
        case 0
            sigma_new= sqrt( drng2 / NumWork );
        case 1
            sigma_new= sqrt( (daz2 + del2) / NumWork );
        case 2
            sigma_new = sqrt( (drng2 + daz2 + del2) / NumWork );
        case 3
            sigma_new= sqrt( (drng2 + daz2 + del2 + ddrng2) / NumWork );
        case 4
            sigma_new= sqrt( (DRng2 + DAz2 + DEl2 + DDRng2 + DDAz2 + DDEl2) / NumWork );
        case 5
            sigma_new= sqrt( (DTRtAsc2 + DTDecl2) / NumWork );
        case 7
            sigma_new= sqrt( DRng2 / NumWork );
    end % Case 
    
end % through iterations    