%{
%-&========================================================================
%-Abstract
%
%   FINDC2C3: this function calculates the c2 and c3 functions for use in 
%             the universal variable calculation of z.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    znew        - z variable                     rad2
%
%   The call:
%      
%      [c2new,c3new] = findc2c3 ( znew );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    c2new       - c2 function value
%    c3new       - c3 function value
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [c2new,c3new] = findc2c3 ( znew );

small =     0.000001;

% -------------------------  implementation   -----------------
if ( znew > small )
    sqrtz = sqrt( znew );
    c2new = (1.0 -cos( sqrtz )) / znew;
    c3new = (sqrtz-sin( sqrtz )) / ( sqrtz^3 );
else
    if ( znew < -small )
        sqrtz = sqrt( -znew );
        c2new = (1.0 -cosh( sqrtz )) / znew;
        c3new = (sinh( sqrtz ) - sqrtz) / ( sqrtz^3 );
    else
        c2new = 0.5;
        c3new = 1.0 /6.0;
    end
end

end
