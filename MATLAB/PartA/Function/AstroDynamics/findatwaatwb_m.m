%{
%-&========================================================================
%-Abstract
%
%   FINDATWBATWA: this procedure finds the a and b matrices for the 
%                 differential correction problem.  remember that it isn't
%                 critical for the propagations to use the highest 
%                 fidelity techniques because we're only trying to find 
%                 the "slope". k is an index that allows us to do multiple
%                 rows at once. it's used for both the b and a matrix 
%                 calculations.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    firstob     - number of observations
%    lastob      - number of observations
%    statesize   - size of state                  6 , 7
%    percentchg  - amount to modify the vectors
%                  by in finite differencing
%    deltaamtchg - tolerance for small value in
%                  finite differencing            0.0000001
%    whichconst  - parameter for sgp4 constants   wgs72, wgs721, wgs84
%    satrec      - structure of satellite parameters for TLE
%    obsrecfile    - array of records containing:
%                  senum, jd, rsvec, obstype,
%                  rng, az, el, drng, daz, del,
%                  trtasc, tdecl data
%    statetype   - type of elements (equinoctial, etc)  'e', 't'
%    scalef      - scale factor to limit the size of the state vector
%    xnom        - state vector                   varied
%
%   The call:
%      
%      [atwa, atwb, atw, b, drng2, daz2, del2 ] = findatwaatwb(firstobs, lastobs, obsrecarr,  statesize, percentchg, deltaamtchg, xnom,re,mu);
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    atwa        - atwa matrix
%    atwb        - atwb matrix
%    atw         - atw matrix
%    b           - b matrix, the residuals
%    drng2       - range residual squared
%    daz2        - azimuth residual squared
%    del2        - elevation residual squared
%    ddrng2      - range rate residual squared
%    ddaz2       - azimuth rate residual squared
%    ddel2       - elevation rate residual squared
%    dtrtasc2    - topocentric right ascension residual squared
%    dtdecl2     - topocentric declination residual squared
%    dx2         - x position residual squared
%    dy2         - y position residual squared
%    dz2         - z position residual squared
%    dxdot2      - xdot position residual squared
%    dydot2      - ydot position residual squared
%    dzdot2      - zdot position residual squared
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [atwa, atwb, atw, b, drng2, daz2, del2 ] = findatwaatwb_m(firstobs, lastobs,sigma,obstype,jd,jdfrac,jdut,jdutfrac,ttt,rs_ecef,site, obsrecarr,  statesize, percentchg, deltaamtchg, xnom,Param);
%%
mu = Param.OrbitalMech.MainAttractor.GM;
GM = mu;
re = Param.OrbitalMech.MainAttractor.R;
flat = Param.OrbitalMech.MainAttractor.Flat;

param = [ GM , re ];

eopdata = Param.OrbitalMech.eopdata;

xp = Param.OrbitalMech.CoorTime.xp;
yp = Param.OrbitalMech.CoorTime.yp;
lod = Param.OrbitalMech.CoorTime.lod;
terms = Param.OrbitalMech.CoorTime.terms;
ddpsi = Param.OrbitalMech.CoorTime.ddpsi; 
ddeps = Param.OrbitalMech.CoorTime.ddeps;

noiserng = sigma.range;
noiseaz = sigma.az;
noiseel = sigma.el;

odeopt = odeset( ...
    'AbsTol'     , 2.22045e-14  ,...
    'RelTol'     , 2.22045e-14  );

%% 
% --------------------- initialize parameters ------------------
if obstype == 0
    indobs = 1;
elseif obstype == 2
    indobs = 3;
else
    indobs = 2;
end

drng2 = 0.0;
daz2 = 0.0;
del2 = 0.0;
dtrtasc2 = 0.0;
dtdecl2 = 0.0;

atwaacc = [0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0];
atwbacc = [0; 0; 0; 0; 0; 0;];

% ------------- reset these since they will accumulate ---------
% zero out matrices
for r = 1: statesize
    for c = 1: statesize
        atwa(r,c) = 0.0;
    end
    atwb(r,1) = 0.0;
end

% ------------------- loop through all the observations ------------------
for obsktr = firstobs: lastobs
    currobsrec = obsrecarr(obsktr);
    jd1_1 = jd(obsktr,1) + jdfrac(obsktr,1);
    jdut1=jdut(obsktr)+jdutfrac(obsktr);
    i_ttt = ttt(obsktr);
    i_rs_ecef = rs_ecef(1:3,obsktr);
    lat_gd = site(obsktr,1);
    lon = site(obsktr,2);
    
    % --------- propagate the nominal vector to the epoch time -----------
    dtsec = (jd(obsktr) + jdfrac(obsktr) - jd(1) - jdfrac(1)) * 86400.0;  % s
    rnom(1) = xnom(1,1);
    rnom(2) = xnom(2,1);
    rnom(3) = xnom(3,1);
    vnom(1) = xnom(4,1);
    vnom(2) = xnom(5,1);
    vnom(3) = xnom(6,1);
%     [reci1, veci1] =  keplerh ( rnom, vnom, dtsec ,mu);
%     [reci1, veci1] =  pkeplerh ( rnom, vnom, dtsec, 0, 0 ,re,mu);
    if dtsec ~= 0
%         [~,X]=ode113(@NewtonEqua,[0,dtsec],[rnom, vnom],odeopt,Param);
        [ sol ] = ode113( @Accel , [0,dtsec] , [rnom, vnom] , odeopt , param );
        X = sol.y';
%         [~,X]=ode113(@ode_kepler,[0,dtsec] , [rnom, vnom],odeopt,mu);
        reci1 = X(end,1:3);
        veci1 = X(end,4:6);
    else
        reci1 = rnom;
        veci1 = vnom;
    end


    % ------------------------- find b matrix ----------------------------
    Mjd_UTC = jd1_1 - Param.OrbitalMech.jd2Mjd;
    [xp,yp,dut1,lod,ddpsi,ddeps,dx_pole,dy_pole,dat] = IERS(eopdata,Mjd_UTC,'l');
    if obstype ~= 3
        [rngnom,aznom,elnom,drngnom,daznom,delnom] = rv2razel_m ( reci1',veci1', i_rs_ecef,lat_gd,lon,i_ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps );
%         [rngnom,aznom,elnom,drngnom,daznom,delnom] = rv2razel_m ( reci1',veci1', currobsrec.vs_ecef',currobsrec.ttt,currobsrec.jdut1,0.0,currobsrec.xp,currobsrec.yp,2,0.0,0.0 );
%         [rngnom,aznom,elnom,drngnom,daznom,delnom] = rv2razel ( reci1',veci1', currobsrec.latgd,currobsrec.lon,currobsrec.alt,currobsrec.ttt,currobsrec.jdut1,0.0,currobsrec.xp,currobsrec.yp,2,0.0,0.0 ,re,flat);
    else
        alt = site(obsktr,3);
        [rngnom,trtascnom,tdeclnom,drngnom,dtrtascnom,dtdeclnom] = rv2tradc ( reci1',veci1', lat_gd,lon,alt,i_ttt,jdut1,lod,xp,yp,2,ddpsi,ddeps ,re,flat);
    end
    
    switch (obstype)
        case 0
            b(1,1) = currobsrec.rng - rngnom;
        case 1
            b(1,1) = currobsrec.az  - aznom;
            %fix for 0-360...
            if (abs(b(1,1)) > pi)
                b(1,1) = b(1,1) - sgn(b(1,1)) * 2.0 * pi;
            end
            b(2,1) = currobsrec.el  - elnom;
        case 2
            b(1,1) = currobsrec.rng  - rngnom;
            b(2,1) = currobsrec.az   - aznom;
            % fix for 0-360...
            if abs(b(2,1)) > pi
                b(2,1) = b(2,1) - sign(b(2,1)) * 2.0 * pi;
            end
            b(3,1) = currobsrec.el  - elnom;
        case 3
            b(1,1) = currobsrec.rtasc - trtascnom;
            % fix for 0-360...
            if (abs(b(1,1)) > pi)
                b(1,1) = b(1,1) - sign(b(1,1)) * 2.0 * pi;
            end
            b(2,1) = currobsrec.decl  - tdeclnom;
    end  % case
    
    % ------------------------ find a matrix -----------------------------
    % ------------- reset the perturbed vector to the nominal ------------
    xnomp = xnom;

    % ----- perturb each element in the state (elements or vectors) ------
    for j= 1 : statesize
        [deltaamt, xnomp] = finitediff(j, percentchg, deltaamtchg, xnom);
        
        dtsec = (jd(obsktr) + jdfrac(obsktr) - jd(1) - jdfrac(1)) * 86400.0;  % s
        rnomp(1) = xnomp(1,1);
        rnomp(2) = xnomp(2,1);
        rnomp(3) = xnomp(3,1);
        vnomp(1) = xnomp(4,1);
        vnomp(2) = xnomp(5,1);
        vnomp(3) = xnomp(6,1);
%         [reci3, veci3] =  keplerh ( rnomp, vnomp, dtsec ,mu);
%         [reci3, veci3] =  pkeplerh ( rnomp, vnomp, dtsec, 0, 0 ,re,mu);
        if dtsec ~= 0
%             [~,X]=ode113(@NewtonEqua,[0,dtsec],[rnomp, vnomp],odeopt,Param);
            [ sol ] = ode113( @Accel , [0,dtsec] , [rnomp, vnomp] , odeopt , param );
            X = sol.y';
%             [~,X]=ode113(@ode_kepler,[0,dtsec] , [rnomp, vnomp],odeopt,mu);
            reci3 = X(end,1:3);
            veci3 = X(end,4:6);
        else
            reci3 = rnomp;
            veci3 = vnomp;
        end

        
        if (obstype == 3)
            [trrpert,trtascpert,tdeclpert,tdrrpert,tdrtascpert,tddeclpert] = rv2tradc( reci3', veci3',lat_gd,lon,alt,ddpsi,ddepsttt,jdut1,lod,xp,yp,2,ddpsi,ddeps ,re,flat);
        else
            if (obstype == 4)
                bstarpert = satrec.bstar*(1.0 + percentchg);
            else  % currobsrec.obstype = 0 or 1 or 2
                [rngpert,azpert,elpert,drhopert,dazpert,delpert] = rv2razel_m ( reci3', veci3',i_rs_ecef,lat_gd,lon,i_ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps );
%                 [rngpert,azpert,elpert,drhopert,dazpert,delpert] = rv2razel_m ( reci3', veci3',currobsrec.rs_ecef',currobsrec.ttt,currobsrec.jdut1,0.0,currobsrec.xp,currobsrec.yp,2,0.0,0.0 );
%                 [rngpert,azpert,elpert,drhopert,dazpert,delpert] = rv2razel ( reci3', veci3', currobsrec.latgd,currobsrec.lon,currobsrec.alt,currobsrec.ttt,currobsrec.jdut1,0.0,currobsrec.xp,currobsrec.yp,2,0.0,0.0 ,re,flat);
                
            end
        end
        
        switch obstype
            case 0
                a(1,j) = (rngpert - rngnom) / deltaamt;
            case 1
                a(1,j) = (azpert  - aznom)  / deltaamt;
                a(2,j) = (elpert  - elnom)  / deltaamt;
            case 2
                a(1,j) = (rngpert - rngnom) / deltaamt;
                a(2,j) = (azpert  - aznom)  / deltaamt;
                a(3,j) = (elpert  - elnom)  / deltaamt;
            case 3
                a(1,j) = (trtascpert - trtascnom) / deltaamt;
                a(2,j) = (tdeclpert  - tdeclnom)  / deltaamt;
        end % case

        % ----------------- reset the modified vector --------------------
        %satrecp = satrec;
        xnomp = xnom;
    end  % for j = 0 to statesize

    % ----------------- now form the matrix combinations -----------------
    at = a';

    % ------------------------- assign weights ---------------------------
    switch (obstype)
        case 0
            w1 = 1.0 / (noiserng * noiserng);
            rng2 = rng2 + b(1,1) * b(1,1) * w1;
        case 1
            w1 = 1.0 / (noiseaz * noiseaz);
            w2 = 1.0 / (noiseel * noiseel);
            daz2 = daz2 + b(1,1) * b(1,1) * w1;
            del2 = del2 + b(2,1) * b(2,1) * w2;
        case 2
            w1 = 1.0 / (noiserng * noiserng);
            w2 = 1.0 / (noiseaz * noiseaz);
            w3 = 1.0 / (noiseel * noiseel);
            drng2 = drng2 + b(1,1) * b(1,1) * w1;
            daz2  = daz2  + b(2,1) * b(2,1) * w2;
            del2  = del2  + b(3,1) * b(3,1) * w3;
        case 3
            w1 = 1.0 / (noisetrtasc * noisetrtasc);
            w2 = 1.0 / (noisetdecl * noisetdecl);
            dtrtasc2 = dtrtasc2 + b(1,1) * b(1,1) * w1;
            dtdecl2  = dtdecl2  + b(2,1) * b(2,1) * w2;
    end % case

    for rowc = 1: statesize
        for colc = 1: indobs
            weight = 1.0;
            switch (colc)
                case 1
                    weight= w1;
                case 2
                    weight= w2;
                case 3
                    weight= w3;
                case 4
                    weight= w4;
                case 5
                    weight= w5;
                case 6
                    weight= w6;
                case 7
                    weight= w7;
            end  % case
            atw(rowc,colc) = at(rowc,colc) * weight;
        end  % for colc
    end % for rowc

    % ----------------- find the atwa / atwb matrices --------------------
    atwaacc = atw * a;
    atwbacc = atw * b;

    % ------------------- accumulate the matricies -----------------------
    for r = 1: statesize
        for c = 1: statesize
            atwa(r,c) = atwaacc(r,c) + atwa(r,c);
        end
    end

    c = 1;
    for r = 1: statesize
        atwb(r,c) = atwbacc(r,c) + atwb(r,c);
    end % for obsktr through the observations

end  % for obsktr through the observations

end
    