%{
%-&========================================================================
%-Abstract
%
%   ANGL: this function calculates the angle between two vectors.  the 
%         output is set to 999999.1 to indicate an undefined value.  be 
%         sure to check for this at the output phase.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    vec1        - vector number 1
%    vec2        - vector number 2
%
%   The call:
%      
%      [theta] = angl ( vec1,vec2 );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    theta       - angle between the two vectors  -pi to pi
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [theta] = angl ( vec1,vec2 );

small     = 0.00000001;
undefined = 999999.1;

magv1 = mag(vec1);
magv2 = mag(vec2);

if magv1*magv2 > small^2
    temp= dot(vec1,vec2) / (magv1*magv2);
    if abs( temp ) > 1.0
        temp= sign(temp) * 1.0;
    end
    theta= acos( temp );
else
    theta= undefined;
end

end
