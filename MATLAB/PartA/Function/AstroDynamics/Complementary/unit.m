%{
%-&========================================================================
%-Abstract
%
%   UINIT: this function calculates a unit vector given the original 
%          vector.  if a zero vector is input, the vector is set to zero.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    vec         - vector
%
%   The call:
%      
%      [outvec] = unit ( vec );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    outvec      - unit vector
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [outvec] = unit ( vec );

% -------------------------  implementation   -----------------
small = 0.000001;
magv = mag(vec);
if ( magv > small )
    for i= 1: 3
        outvec(i)= vec(i)/magv;
     end
else
    for i= 1: 3
        outvec(i)= 0.0;
     end
end

