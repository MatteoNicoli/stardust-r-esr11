%{
%-&========================================================================
%-Abstract
%
%   ROT3: this function performs a rotation about the 3rd axis.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    vec         - input vector
%    xval        - angle of rotation              rad
%
%   The call:
%      
%      [outvec] = rot3 ( vec, xval );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    outvec      - vector result
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [outvec] = rot3 ( vec, xval );

temp= vec(2);
c= cos( xval );
s= sin( xval );

outvec(2)= c*vec(2) - s*vec(1);
outvec(1)= c*vec(1) + s*temp;
outvec(3)= vec(3);

end
