%{
%-&========================================================================
%-Abstract
%
%   MAG: this function finds the magnitude of a vector.  the tolerance is 
%        set to 0.000001, thus the 1.0e-12 for the squared test of 
%        underflows.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    vec         - vector
%
%   The call:
%      
%      mag = mag ( vec );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    mag         - magnitude
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function mag = mag ( vec );

        temp= vec(1)*vec(1) + vec(2)*vec(2) + vec(3)*vec(3);

        if abs( temp ) >= 1.0e-16
            mag= sqrt( temp );
          else
            mag= 0.0;
          end

