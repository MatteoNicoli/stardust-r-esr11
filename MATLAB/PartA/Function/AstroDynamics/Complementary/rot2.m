%{
%-&========================================================================
%-Abstract
%
%   ROT2: this function performs a rotation about the 2nd axis.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    vec         - input vector
%    xval        - angle of rotation              rad
%
%   The call:
%      
%      [outvec] = rot2 ( vec, xval );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    outvec      - vector result
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [outvec] = rot2 ( vec, xval );

temp= vec(3);
c= cos( xval );
s= sin( xval );

outvec(3)= c*vec(3) + s*vec(1);
outvec(1)= c*vec(1) - s*temp;
outvec(2)= vec(2);

end

