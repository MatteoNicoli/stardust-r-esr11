%{
%-&========================================================================
%-Abstract
%
%   RAZ2RVS: this function converts range, azimuth, and elevation values 
%            with slant range and velocity vectors for a satellite from a 
%            radar site in the topocentric horizon (sez) system.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    rho         - satellite range from site      km
%    az          - azimuth                        0.0 to 2pi rad
%    el          - elevation                      -pi/2 to pi/2 rad
%    drho        - range rate                     km / s
%    daz         - azimuth rate                   rad / s
%    del         - elevation rate                 rad / s
%
%   The call:
%      
%      [rhosez,drhosez] =  raz2rvs ( rho,az,el,drho,daz,del );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    rhovec      - sez satellite range vector     km
%    drhovec     - sez satellite velocity vector  km / s
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [rhosez,drhosez] =  raz2rvs ( rho,az,el,drho,daz,del );

% ----------------------- initialize values -------------------
sinel= sin(el);
cosel= cos(el);
sinaz= sin(az);
cosaz= cos(az);

% ------------------- form sez range vector -------------------
rhosez(1) = -rho*cosel*cosaz;
rhosez(2) =  rho*cosel*sinaz;
rhosez(3) =  rho*sinel;

% ----------------- form sez velocity vector ------------------
drhosez(1) = -drho*cosel*cosaz + rhosez(3)*del*cosaz + rhosez(2)*daz;
drhosez(2) =  drho*cosel*sinaz - rhosez(3)*del*sinaz - rhosez(1)*daz;
drhosez(3) =  drho*sinel       + rho*del*cosel;

end