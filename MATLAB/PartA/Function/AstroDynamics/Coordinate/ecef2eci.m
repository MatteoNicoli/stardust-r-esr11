%{
%-&========================================================================
%-Abstract
%
%   ECEF2ECI: this function transforms a vector from the earth fixed (itrf)
%              frame, to the eci mean equator mean equinox (j2000).
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    recef       - position vector earth fixed    km
%    vecef       - velocity vector earth fixed    km/s
%    aecef       - acceleration vector earth fixedkm/s2
%    ttt         - julian centuries of tt         centuries
%    jdut1       - julian date of ut1             days from 4713 bc
%    lod         - excess length of day           sec
%    xp          - polar motion coefficient       rad
%    yp          - polar motion coefficient       rad
%    eqeterms    - terms for ast calculation      0,2
%    ddpsi       - delta psi correction to gcrf   rad
%    ddeps       - delta eps correction to gcrf   rad
%
%   The call:
%      
%      [reci,veci,aeci] = ecef2eci  ( recef,vecef,aecef,ttt,jdut1,lod,xp,yp,eqeterms,ddpsi,ddeps );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    reci        - position vector eci            km
%    veci        - velocity vector eci            km/s
%    aeci        - acceleration vector eci        km/s2
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [reci,veci,aeci] = ecef2eci  ( recef,vecef,aecef,ttt,jdut1,lod,xp,yp,eqeterms,ddpsi,ddeps );

% ---- find matrices
[prec,psia,wa,ea,xa] = precess ( ttt, '80' );

[deltapsi,trueeps,meaneps,omega,nut] = nutation(ttt,ddpsi,ddeps);

[st,stdot] = sidereal(jdut1,deltapsi,meaneps,omega,lod,eqeterms );

[pm] = polarm(xp,yp,ttt,'80');

% ---- perform transformations
thetasa= 7.29211514670698e-05 * (1.0  - lod/86400.0 );
omegaearth = [0; 0; thetasa;];

rpef = pm*recef;
reci = prec*nut*st*rpef;

vpef = pm*vecef;
veci = prec*nut*st*(vpef + cross(omegaearth,rpef));

% veci1 = prec*nut * (stdot*recef + st*pm*vecef)  % alt approach using sidereal rate

temp = cross(omegaearth,rpef);
aeci = prec*nut*st*( pm*aecef + cross(omegaearth,temp) ...
       + 2.0*cross(omegaearth,vpef) );

end