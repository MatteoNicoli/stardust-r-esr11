%{
%-&========================================================================
%-Abstract
%
%   GD2GC: this function converts from geodetic to geocentric latitude for 
%          positions on the surface of the earth.  notice that (1-f) 
%          squared = 1-esqrd.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    latgd       - geodetic latitude              -pi to pi rad
%
%   The call:
%      
%      [latgc] = gd2gc ( latgd );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    latgc       - geocentric latitude            -pi to pi rad
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [latgc] = gd2gc ( latgd );

        eesqrd = 0.006694385000;     % eccentricity of earth sqrd

        % -------------------------  implementation   -----------------
        latgc= atan( (1.0  - eesqrd)*tan(latgd) );

