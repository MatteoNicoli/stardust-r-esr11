%{
%-&========================================================================
%-Abstract
%
%   GC2GD: this function converts from geodetic to geocentric latitude for 
%          positions on the surface of the earth.  notice that (1-f) 
%          squared = 1-esqrd.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    latgd       - geodetic latitude              -pi to pi rad
%
%   The call:
%      
%      [latgd] = gc2gd ( latgc );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    latgc       - geocentric latitude            -pi to pi rad
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [latgd] = gc2gd ( latgc );

eesqrd = 0.006694385000;     % eccentricity of earth sqrd

% -------------------------  implementation   -----------------
latgd= atan( tan(latgc)/(1.0  - eesqrd) );

end