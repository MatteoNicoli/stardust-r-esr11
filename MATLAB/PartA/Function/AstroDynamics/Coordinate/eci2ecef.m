%{
%-&========================================================================
%-Abstract
%
%   ECI2ECEF: this function trsnforms a vector from the mean equator mean 
%             equniox frame (j2000), to an earth fixed (ITRF) frame.  the 
%             results take into account the effects of precession, 
%             nutation, sidereal time, and polar motion.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    reci        - position vector eci            km
%    veci        - velocity vector eci            km/s
%    aeci        - acceleration vector eci        km/s2
%    ttt         - julian centuries of tt         centuries
%    jdut1       - julian date of ut1             days from 4713 bc
%    lod         - excess length of day           sec
%    xp          - polar motion coefficient       arc sec
%    yp          - polar motion coefficient       arc sec
%    eqeterms    - terms for ast calculation      0,2
%    ddpsi       - delta psi correction to gcrf   rad
%    ddeps       - delta eps correction to gcrf   rad
%
%   The call:
%      
%      [recef,vecef,aecef] = eci2ecef  ( reci,veci,aeci,ttt,jdut1,lod,xp,yp,eqeterms,ddpsi,ddeps );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    recef       - position vector earth fixed    km
%    vecef       - velocity vector earth fixed    km/s
%    aecef       - acceleration vector earth fixedkm/s2
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [recef,vecef,aecef] = eci2ecef  ( reci,veci,aeci,ttt,jdut1,lod,xp,yp,eqeterms,ddpsi,ddeps );

[prec,psia,wa,ea,xa] = precess ( ttt, '80' );

[deltapsi,trueeps,meaneps,omega,nut] = nutation(ttt,ddpsi,ddeps);

[st,stdot] = sidereal(jdut1,deltapsi,meaneps,omega,lod,eqeterms );

[pm] = polarm(xp,yp,ttt,'80');

thetasa= 7.29211514670698e-05 * (1.0  - lod/86400.0 );
omegaearth = [0; 0; thetasa;];

rpef  = st'*nut'*prec'*reci;
recef = pm'*rpef;

vpef  = st'*nut'*prec'*veci - cross( omegaearth,rpef );
vecef = pm'*vpef;

temp  = cross(omegaearth,rpef);

aecef = pm'*(st'*nut'*prec'*aeci - cross(omegaearth,temp) ...
        - 2.0*cross(omegaearth,vpef));

end