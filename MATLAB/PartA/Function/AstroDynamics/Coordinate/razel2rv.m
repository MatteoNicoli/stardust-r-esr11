%{
%-&========================================================================
%-Abstract
%
%   RAZEL2RV: this function converts range, azimuth, and elevation and 
%             their rates to the geocentric equatorial (eci) position and 
%             velocity vectors.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    rho         - satellite range from site      km
%    az          - azimuth                        0.0 to 2pi rad
%    el          - elevation                      -pi/2 to pi/2 rad
%    drho        - range rate                     km/s
%    daz         - azimuth rate                   rad / s
%    del         - elevation rate                 rad / s
%    rs          - ecef site position vector      km
%    latgd       - geodetic latitude              -pi/2 to pi/2 rad
%    lon         - longitude of site              -2pi to 2pi rad
%    alt         - altitude                       km
%    ttt         - julian centuries of tt         centuries
%    jdut1       - julian date of ut1             days from 4713 bc
%    lod         - excess length of day           sec
%    xp          - polar motion coefficient       arc sec
%    yp          - polar motion coefficient       arc sec
%    terms       - number of terms for ast calculation 0,2
%
%   The call:
%      
%      [reci,veci] = razel2rv ( rho,az,el,drho,daz,del,latgd,lon,alt,ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    reci        - eci position vector            km
%    veci        - eci velocity vector            km/s
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [reci,veci] = razel2rv ( rho,az,el,drho,daz,del,latgd,lon,alt,ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps ,re ,flat);

% -------------------------  implementation   -----------------
constmath;

% -----------  find sez range and velocity vectors ------------
[rhosez,drhosez] = raz2rvs( rho,az,el,drho,daz,del );

% -----------  perform sez to ijk (ecef) transformation -------
[tempvec] = rot2( rhosez , latgd-halfpi );
[rhoecef] = rot3( tempvec,-lon          );
rhoecef = rhoecef';

[tempvec] = rot2( drhosez, latgd-halfpi );
[drhoecef]= rot3( tempvec,-lon          );
drhoecef = drhoecef';

% ----------  find ecef range and velocity vectors -------------
[rs,vs] = siteh ( latgd,lon,alt ,re ,flat);
recef = rhoecef + rs;
vecef = drhoecef;

% -------- convert ecef to eci
recef = recef;
vecef = vecef;
a     = [0;0;0];
[reci,veci,aeci] = ecef2eci(recef,vecef,a,ttt,jdut1,lod,xp,yp,terms,ddpsi,ddeps );

end
