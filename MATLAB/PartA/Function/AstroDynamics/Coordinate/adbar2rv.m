%{
%-&========================================================================
%-Abstract
%
%   ADBAR2RV: this function transforms the adbarv elements (rtasc, decl, 
%             fpa, azimuth, position and velocity magnitude) into eci 
%             position and velocity vectors.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    rmag        - eci position vector magnitude  km
%    vmag        - eci velocity vector magnitude  km/sec
%    rtasc       - right ascension of sateillite  rad
%    decl        - declination of satellite       rad
%    fpav        - sat flight path angle from vertrad
%    az          - sat flight path azimuth        rad
%
%   The call:
%      
%      [r,v] = adbar2rv ( rmag,vmag,rtasc,decl,fpav,az );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    r           - eci position vector            km
%    v           - eci velocity vector            km/s
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [r,v] = adbar2rv ( rmag,vmag,rtasc,decl,fpav,az );

        % -------- form position vector
        r(1)= rmag*cos(decl)*cos(rtasc);
        r(2)= rmag*cos(decl)*sin(rtasc);
        r(3)= rmag*sin(decl);

        % -------- form velocity vector
        v(1)= vmag*( cos(rtasc)*(-cos(az)*sin(fpav)*sin(decl) + ...
                     cos(fpav)*cos(decl)) - sin(az)*sin(fpav)*sin(rtasc) );
        v(2)= vmag*( sin(rtasc)*(-cos(az)*sin(fpav)*sin(decl) + ...
                     cos(fpav)*cos(decl)) + sin(az)*sin(fpav)*cos(rtasc) );
        v(3)= vmag*( cos(az)*cos(decl)*sin(fpav) + cos(fpav)*sin(decl) );

        r = r';
        v = v';

