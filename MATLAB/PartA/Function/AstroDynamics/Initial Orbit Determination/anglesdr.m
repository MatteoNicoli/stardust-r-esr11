%{
%-&========================================================================
%-Abstract
%
%   ANGLESDR: this function solves the problem of orbit determination using
%             three optical sightings.  the solution function uses the 
%             double-r technique.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    rtasc1       - right ascension #1            rad
%    rtasc2       - right ascension #2            rad
%    rtasc3       - right ascension #3            rad
%    decl1        - declination #1                rad
%    decl2        - declination #2                rad
%    decl3        - declination #3                rad
%    jd1          - julian date of 1st sighting   days from 4713 bc
%    jd2          - julian date of 2nd sighting   days from 4713 bc
%    jd3          - julian date of 3rd sighting   days from 4713 bc
%    rs           - ijk site position vector      km
%    re
%    mu
%
%   The call:
%      
%      [r2,v2,r1,r3] = anglesdr( decl1,decl2,decl3,rtasc1,rtasc2,rtasc3,jd1,jd2,jd3,rsite1,rsite2,rsite3, re, mu)
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    r            - ijk position vector at t2     km
%    v            - ijk velocity vector at t2     km / s
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [r2,v2,r1,r3] = anglesdr ( decl1,decl2,decl3,rtasc1,rtasc2, ...
                              rtasc3,jd1,jd2,jd3,rsite1,rsite2,rsite3, re, mu)

% -------------------------  implementation   -------------------------

tu = 24 * 3600;

magr1in = 2.0*re;
magr2in = 2.01*re;
direct  = 'y';

tol    = 1e-8*re;
pctchg = 0.005;

% subtract dates and convert fraction of day to seconds
t1 = (jd1 - jd2)*tu;  % secs
t3 = (jd3 - jd2)*tu;

% form line of sight vectors
los1 = [cos(decl1)*cos(rtasc1) cos(decl1)*sin(rtasc1) sin(decl1)]';
los2 = [cos(decl2)*cos(rtasc2) cos(decl2)*sin(rtasc2) sin(decl2)]';
los3 = [cos(decl3)*cos(rtasc3) cos(decl3)*sin(rtasc3) sin(decl3)]';

% --------- now we're ready to start the actual double r algorithm --------
magr1old  = 99999.9;
magr2old  = 99999.9;
magr3old  = 99999.9;
magrsite1 = mag(rsite1);
magrsite2 = mag(rsite2);
magrsite3 = mag(rsite3);

% take away negatives because escobal defines rs opposite
cc1 = 2.0*dot(los1,rsite1);
cc2 = 2.0*dot(los2,rsite2);
cc3 = 2.0*dot(los3,rsite3);
ktr = 0;

% main loop to get three values of the double-r for processing
while (abs(magr1in-magr1old) > tol | abs(magr2in-magr2old) > tol)
    ktr = ktr + 1;
    
    [r1,r2,r3,f1,f2,q1,magr1,magr2,a,deltae32] = doubler( cc1,cc2,magrsite1,magrsite2,magr1in,magr2in,...
                   los1,los2,los3,rsite1,rsite2,rsite3,t1,t3,direct, re, mu);

   % -------------- re-calculate f1 and f2 with r1 = r1 + delta r1
   magr1o = magr1in;
   magr1in = (1.0+pctchg)*magr1in;
   deltar1 = pctchg*magr1in;
   [r1,r2,r3,f1delr1,f2delr1,q2,magr1,magr2,a,deltae32] = doubler( cc1,cc2,magrsite1,magrsite2,magr1in,magr2in,...
                          los1,los2,los3,rsite1,rsite2,rsite3,t1,t3,direct, re, mu);
   pf1pr1 = (f1delr1-f1)/deltar1;
   pf2pr1 = (f2delr1-f2)/deltar1;

   % ----------------  re-calculate f1 and f2 with r2 = r2 + delta r2
   magr1in = magr1o;
   deltar1 = pctchg*magr1in;
   magr2o = magr2in;
   magr2in = (1.0+pctchg)*magr2in;
   deltar2 = pctchg*magr2in;
   [r1,r2,r3,f1delr2,f2delr2,q3,magr1,magr2,a,deltae32] = doubler( cc1,cc2,magrsite1,magrsite2,magr1in,magr2in,...
                          los1,los2,los3,rsite1,rsite2,rsite3,t1,t3,direct, re, mu);
   pf1pr2 = (f1delr2-f1)/deltar2;
   pf2pr2 = (f2delr2-f2)/deltar2;

   % ------------ now calculate an update
   magr2in = magr2o;
   deltar2 = pctchg*magr2in;

   delta  = pf1pr1*pf2pr2 - pf2pr1*pf1pr2;
   delta1 = pf2pr2*f1 - pf1pr2*f2;
   delta2 = pf1pr1*f2 - pf2pr1*f1;

   deltar1 = -delta1/delta;
   deltar2 = -delta2/delta;

   magr1old = magr1in;
   magr2old = magr2in;

   %  may need to limit the amount of the correction
   if abs(deltar1) > magr1in*pctchg
       deltar1 = sign(deltar1)*magr1in*pctchg;
   end
   if abs(deltar2) > magr2in*pctchg
       deltar2 = sign(deltar2)*magr2in*pctchg;
   end

   magr1in = magr1in + deltar1;
   magr2in = magr2in + deltar2;

end

% needed to get the r2 set properly since the last one was moving r2
[r1,r2,r3,f1,f2,q1,magr1,magr2,a,deltae32] = doubler( cc1,cc2,magrsite1,magrsite2,magr1in,magr2in,...
                      los1,los2,los3,rsite1,rsite2,rsite3,t1,t3,direct, re, mu);

f  = 1.0 - a/magr2*(1.0-cos(deltae32));
g  = t3 - sqrt(a^3/mu)*(deltae32-sin(deltae32));
v2 = (r3 - f*r2)/g;


end