%{
%-&========================================================================
%-Abstract
%
%   HGIBBS: this function implements the herrick-gibbs approximation for 
%           orbit determination, and finds the middle velocity vector for 
%           the 3 given position vectors.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    r1          - ijk position vector #1         km
%    r2          - ijk position vector #2         km
%    r3          - ijk position vector #3         km
%    use seconds to provide more accuracy
%    t1         - time julian date of 1st sighting    days from 4713 bc sec
%    t2         - time julian date of 2nd sighting    days from 4713 bc sec
%    t3         - time julian date of 3rd sighting    days from 4713 bc sec
%
%   The call:
%      
%      [v2, theta,theta1,copa, error ] =  hgibbs ( r1,r2,r3,jd1,jd2,jd3 ,mu);
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    v2          - ijk velocity vector for r2     km / s
%    theta       - angl between vectors          rad
%    error       - flag indicating success        'ok',...
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [v2, theta,theta1,copa, error ] =  hgibbs_m ( r1,r2,r3,dt21,dt32 ,mu);

% -------------------------  implementation   -------------------------
constmath;

error =  '          ok';
theta = 0.0;
theta1= 0.0;
magr1 = mag( r1 );
magr2 = mag( r2 );
magr3 = mag( r3 );
for i= 1 : 3
    v2(i)= 0.0;
end

tolangle= 0.01745329251994;
% dt21= (jd2-jd1)*86400.0;
% dt31= (jd3-jd1)*86400.0;    % differences in times in secs
% dt32= (jd3-jd2)*86400.0;
dt31 = dt21 + dt32;


p = cross( r2,r3 );
pn = unit( p );
r1n = unit( r1 );
copa=  asin( dot( pn,r1n ) );
if ( abs( dot(r1n,pn) ) > 0.017452406 )
    error= 'not coplanar';
end

% --------------------------------------------------------------
%       check the size of the angles between the three position vectors.
%       herrick gibbs only gives "reasonable" answers when the
%       position vectors are reasonably close.  10 deg is only an estimate.
% --------------------------------------------------------------
theta  = angl( r1,r2 );
theta1 = angl( r2,r3 );
if ( (theta > tolangle) | (theta1 > tolangle) )  
    error= '   angl > 1�';
end

% ----------- perform herrick-gibbs method to find v2 ---------
term1= -dt32*( 1.0/(dt21*dt31) + mu/(12.0*magr1*magr1*magr1) );
term2= (dt32-dt21)*( 1.0/(dt21*dt32) + mu/(12.0*magr2*magr2*magr2) );
term3=  dt21*( 1.0/(dt32*dt31) + mu/(12.0*magr3*magr3*magr3) );

v2 =  term1*r1 + term2* r2 + term3* r3;

end
