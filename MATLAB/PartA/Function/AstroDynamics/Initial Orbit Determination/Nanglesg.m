%{
%-&========================================================================
%-Abstract
%
%   ANGLESG: this function solves the problem of orbit determination using 
%            three optical sightings.  the solution function uses the 
%            gaussian technique. there are lots of debug statements in here
%            to test various options.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    R            - ijk site position vector      -
%    rho_vet      - ijk observation position vector
%    t            - time vector
%    mu           - grav param earth, sun etc    
%
%   The call:
%      
%      [ r_target , v_target , r ] = Nanglesg( R , rho_vet , t , mu );
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    r_target        - ijk position vector at t_k+1     -
%    v_target        - ijk velocity vector at t_k+1     -
     r               - norm r_target
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [ r_target , v_target , r ] = Nanglesg( R , rho_vet , t , mu )
%%
% Optfsolve = optimoptions_m( ...
%     'fsolve' , ...
%     'Display' , 'iter' , ...
%     'Algorithm' , 'Levenberg-Marquardt' );

% Optfsolve = optimoptions( ...
%     'fsolve' , ...
%     'Display' , 'iter' , ...
%     'Algorithm' , 'Levenberg-Marquardt' , ...
%     'FunctionTolerance' , 1e-20 , ...
%     'OptimalityTolerance' , 1e-20 );

% Optfsolve2 = optimoptions( ...
%     'fsolve' , ...
%     'Display' , 'off' , ...
%     'Algorithm' , 'Levenberg-Marquardt' , ...
%     'FunctionTolerance' , 1e-20 , ...
%     'OptimalityTolerance' , 1e-20 );

%%
% [ row , col ] = size( rho_vet );
% dtk( : , 1 ) = t( 2 : end - 1 ) - t( 1 : end - 2 );
% dtkp1( : , 1 ) = t( 3 : end ) - t( 2 : end - 1 );

IC = 0 .* ones( size( rho_vet , 1 ) , 1 );

%%
[ rho_c , phi_c ] = newtonsys (@fun ,@Jfun ,IC ,1e-6 ,1000 );
% [ rho_c , phi_c ] = fsolve_m( @fun , IC );
% [ rho_c , phi_c ] = fsolve( @fun , IC );
% phi_c
r_target=zeros(length(rho_c),3);
for iik = 1 : length(rho_c)
    r_target(iik,:) = R(iik,:) + rho_c(iik) .* rho_vet(iik,:);
end
r = sqrt( sum( r_target.^2 , 2 ) );

for ik = 2 : size( r_target , 1 ) - 1
    dtk_c = t( ik ) - t( ik - 1 );
    dtk1_c = t( ik + 1 ) - t( ik );
    
    rkm1_c =  R( ik - 1 , : )+rho_c( ik - 1 ) .* rho_vet( ik - 1 , : );
    r_k_c = R( ik , : )+rho_c( ik ) .* rho_vet( ik , : );
    rkp1_c = R( ik + 1 , : )+rho_c( ik + 1 ) .* rho_vet( ik + 1 , : );
        
    [v_k] = hgibbs_m(rkm1_c',r_k_c',rkp1_c',dtk_c,dtk1_c,mu);
    
    v_target( ik - 1 , : ) = v_k;
end

%%
function [ f ] = fun( x , varargin )
    
    rho = x;
    phi = [  ];
    
    for k = 2 : size( rho_vet , 1 ) - 1
        dtk = t( k ) - t( k - 1 );
        dtkp1 = t( k + 1 ) - t( k );
        
        rkm1 =  R( k - 1 , : )+rho( k - 1 ) .* rho_vet( k - 1 , : );
        r_k = R( k , : )+rho( k ) .* rho_vet( k , : );
        rkp1 = R( k + 1 , : )+rho( k + 1 ) .* rho_vet( k + 1 , : );
        
        rk = norm( r_k );
        
        [v_k] = hgibbs_m(rkm1',r_k',rkp1',dtk,dtkp1,mu);
        vk = norm( v_k );
        
        fkm1_i = 1 - mu ./ ( 2 .* rk.^3 ) .* dtk.^2-mu.*dot(r_k,v_k)./(2.*rk.^5).*dtk.^3+mu./24.*(-2.*mu./rk.^6+3.*vk.^2./rk.^5-15.*dot(r_k,v_k).^2./rk.^7).*dtk.^4;
        fkp1_i = 1 - mu ./ ( 2 .* rk.^3 ) .* dtkp1.^2+mu.*dot(r_k,v_k)./(2.*rk.^5).*dtkp1.^3+mu./24.*(-2.*mu./rk.^6+3.*vk.^2./rk.^5-15.*dot(r_k,v_k).^2./rk.^7).*dtk.^4;

        gkm1_i = - dtk + mu ./ ( 6 .* rk.^3  ) .* dtk.^3 + mu.*dot(r_k,v_k)./(4.*rk.^5).*dtk.^4;
        gkp1_i = dtkp1 - mu ./ ( 6 .* rk.^3  ) .* dtkp1.^3 + mu.*dot(r_k,v_k)./(4.*rk.^5).*dtkp1.^4;
        
        c( k ) = gkp1_i ./ ( fkm1_i .* gkp1_i - fkp1_i .* gkm1_i );
        d( k ) = - gkm1_i ./ ( fkm1_i .* gkp1_i - fkp1_i .* gkm1_i );
        
%         c( k ) = dtk1 ./ ( dtk + dtk1 ) .* ( 1 + mu .* ( ( dtk + dtk1 ).^2 - dtk1.^2 ) ./ ( 6 .* rk.^3 )  );
%         d( k ) = dtk ./ ( dtk + dtk1 ) .* ( 1 + mu .* ( ( dtk + dtk1 ).^2 - dtk.^2 ) ./ ( 6 .* rk.^3 )  );
        
        iphi( 1 , 1 ) = c( k ).*( R(k-1,1)+rho(k-1).*rho_vet(k-1,1) )+d(k).*(R(k+1,1)+rho(k+1).*rho_vet(k+1,1))-(R(k,1)+rho(k).*rho_vet(k,1));
        iphi( 2 , 1 ) = c( k ).*( R(k-1,2)+rho(k-1).*rho_vet(k-1,2) )+d(k).*(R(k+1,2)+rho(k+1).*rho_vet(k+1,2))-(R(k,2)+rho(k).*rho_vet(k,2));
        iphi( 3 , 1 ) = c( k ).*( R(k-1,3)+rho(k-1).*rho_vet(k-1,3) )+d(k).*(R(k+1,3)+rho(k+1).*rho_vet(k+1,3))-(R(k,3)+rho(k).*rho_vet(k,3));
        
        phi = [ phi ; iphi ];
    end
    
    f = phi;
    
end

function [ Jf ] = Jfun( x , varargin )
    
    rho = x;
    
    for i = 1 : size( rho_vet , 1 ) - 2
        k = i + 1;
        dtk = t( k ) - t( k - 1 );
        dtkp1 = t( k + 1 ) - t( k );
        
        rkm1 =  R( k - 1 , : )+rho( k - 1 ) .* rho_vet( k - 1 , : );
        r_k = R( k , : )+rho( k ) .* rho_vet( k , : );
        rkp1 = R( k + 1 , : )+rho( k + 1 ) .* rho_vet( k + 1 , : );
        
        rk = norm( r_k );
        
        [v_k] = hgibbs_m(rkm1',r_k',rkp1',dtk,dtkp1,mu);
        vk = norm( v_k );
        
        fkm1_i = 1 - mu ./ ( 2 .* rk.^3 ) .* dtk.^2-mu.*dot(r_k,v_k)./(2.*rk.^5).*dtk.^3+mu./24.*(-2.*mu./rk.^6+3.*vk.^2./rk.^5-15.*dot(r_k,v_k).^2./rk.^7).*dtk.^4;
        fkp1_i = 1 - mu ./ ( 2 .* rk.^3 ) .* dtkp1.^2+mu.*dot(r_k,v_k)./(2.*rk.^5).*dtkp1.^3+mu./24.*(-2.*mu./rk.^6+3.*vk.^2./rk.^5-15.*dot(r_k,v_k).^2./rk.^7).*dtk.^4;

        gkm1_i = - dtk + mu ./ ( 6 .* rk.^3  ) .* dtk.^3 + mu.*dot(r_k,v_k)./(4.*rk.^5).*dtk.^4;
        gkp1_i = dtkp1 - mu ./ ( 6 .* rk.^3  ) .* dtkp1.^3 + mu.*dot(r_k,v_k)./(4.*rk.^5).*dtkp1.^4;
        
        c( k ) = gkp1_i ./ ( fkm1_i .* gkp1_i - fkp1_i .* gkm1_i );
%         d( k ) = - gkm1_i ./ ( fkm1_i .* gkp1_i - fkp1_i .* gkm1_i );
        
%         c = 1 / 2 + mu / ( 4 * rk.^3 ) * dtk^2;
        dcdrho = - 3 * mu / ( 4 * rk.^5 ) * ( dot( rho_vet( k , : )' , r_k ) );
        J( 3 * i - 2 : 3 * i , i ) = c( k ) * rho_vet( i , : );
        J( 3 * i - 2 : 3 * i , i + 1 ) = dcdrho * ( rkm1 - rkp1 ) - rho_vet( i , : );
        J( 3 * i - 2 : 3 * i , i + 2 ) = c( k ) * rho_vet( i + 2 , : );
        
        
    end
    Jf = J;
end
end