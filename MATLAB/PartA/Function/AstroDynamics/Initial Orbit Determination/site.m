%{
%-&========================================================================
%-Abstract
%
%   SITE: this function finds the position and velocity vectors for a 
%         site.  the answer is returned in the geocentric equatorial 
%         (ecef) coordinate system. Note that the velocity is zero 
%         because the coordinate system is fixed to the earth.
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    latgd       - geodetic latitude              -pi/2 to pi/2 rad
%    lon         - longitude of site              -2pi to 2pi rad
%    alt         - altitude                       km
%
%   The call:
%      
%      [rs,vs] = site ( latgd,lon,alt ,re ,flat);
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    rs          - ecef site position vector      km
%    vs          - ecef site velocity vector      km/s
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [rs,vs] = site ( latgd,lon,alt ,re ,flat);
        
% derived constants from the base values
eccearth = sqrt(2.0*flat - flat^2);
eccearthsqrd = eccearth^2;

% -------------------------  implementation   -----------------
sinlat      = sin( latgd );

% ------  find rdel and rk components of site vector  ---------
cearth= re / sqrt( 1.0 - ( eccearthsqrd*sinlat*sinlat ) );
rdel  = ( cearth + alt )*cos( latgd );
rk    = ( (1.0-eccearthsqrd)*cearth + alt )*sinlat;

% ---------------  find site position vector  -----------------
rs(1) = rdel * cos( lon );
rs(2) = rdel * sin( lon );
rs(3) = rk;
rs = rs';

% ---------------  find site velocity vector  -----------------
[vs] = [0.0; 0.0; 0.0];

end
