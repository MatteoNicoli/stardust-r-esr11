function [dXdt] = ode_kepler( t , X , mu )

x = X(1);
y = X(2);
z = X(3);
x_d = X(4);
y_d = X(5);
z_d = X(6);

r_norm = sqrt( x^2 + y^2 + z^2 );

x_dd = -mu*x/r_norm^3;
y_dd = -mu*y/r_norm^3;
z_dd = -mu*z/r_norm^3;

dXdt(1,1) = x_d;
dXdt(2,1) = y_d;
dXdt(3,1) = z_d;
dXdt(4,1) = x_dd;
dXdt(5,1) = y_dd;
dXdt(6,1) = z_dd;
end