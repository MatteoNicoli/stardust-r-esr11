%--------------------------------------------------------------------------
%
% VarEqn: Computes the variational equations, i.e. the derivative of the
%         state vector and the state transition matrix
%
% Input:
%   x           Time since epoch AuxParam.Mjd_0 in [s]
%   yPhiS       (6+36+6)-dim vector comprising the state vector (y), the
%               state transition matrix (Phi) and the sensitivity matrix
%               in column wise storage order
%
% Output:
%   yPhip      Derivative of yPhi
%
% Last modified:   2015/08/12   M. Mahooti
%
%--------------------------------------------------------------------------
function yPhip = VarEqn(x, yPhi)

% gm = param(1);
% r_ref =param(2);
gm    = 398600.4415e9;              % [m^3/s^2]; ITG-Grace03s
r_ref = 6378.1366e3;                % Radius Earth [m]; ITG-Grace03s

global AuxParam eopdata

% [UT1_UTC,TAI_UTC,x_pole,y_pole,ddpsi,ddeps] = IERS(eopdata,AuxParam.Mjd_UTC+x/86400,'l');
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,AuxParam.Mjd_UTC+x/86400,'l');
Mjd_UT1 = AuxParam.Mjd_UTC+x/86400+UT1_UTC/86400;

% Transformation matrix
U = R_z(gmst(Mjd_UT1));
 
% State vector components
r = yPhi(1:3);
v = yPhi(4:6);
Phi = zeros(6);

% State transition matrix
for j=1:6
    Phi(:,j) = yPhi(6*j+1:6*j+6);
end

% Acceleration and gradient
a = AccelHarmonic(r, U, AuxParam.n_a, AuxParam.m_a,gm,r_ref);
G = Gradient(r, U, AuxParam.n_G, AuxParam.m_G,gm,r_ref);

% Time derivative of state transition matrix
yPhip = zeros(42,1);
dfdy = zeros(6);

for i=1:3
  for j=1:3
    dfdy(i,j) = 0.0;               % dv/dr(i,j)
    dfdy(i+3,j) = G(i,j);          % da/dr(i,j)
    if ( i==j ) 
        dfdy(i,j+3) = 1; 
    else
        dfdy(i,j+3) = 0;           % dv/dv(i,j)
    end
    dfdy(i+3,j+3) = 0;             % da/dv(i,j)
  end
end

Phip = dfdy*Phi;

% Derivative of combined state vector and state transition matrix
for i=1:3
  yPhip(i)   = v(i);               % dr/dt(i)
  yPhip(i+3) = a(i);               % dv/dt(i)
end

for i=1:6
  for j=1:6
    yPhip(6*j+i) = Phip(i,j);      % dPhi/dt(i,j)
  end
end
  
