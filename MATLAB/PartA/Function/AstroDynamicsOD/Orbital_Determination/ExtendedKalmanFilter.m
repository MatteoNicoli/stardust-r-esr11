%{
%-&========================================================================
%-Abstract
%
%   OrbitDet_DiffCorLSQ: Extended Kalman Filter for orbital determination
%
%-Author
%
%   Matteo Nicoli
%
%-Disclaimer
%
%   THIS SOFTWARE AND ANY RELATED MATERIALS WERE CREATED BY MATTEO NICOLI.
%
%-I/O
%
%   Given:
%      
%    Inputs       Description                     type [units]
%    r_obs          observation postion vector
%    jd             julian date
%    jdfrac         fraction of julian date
%    jdut           corrected julian date
%    jdutfrac       corrected fraction of julian date
%    ttt            julian centuries
%    rs_ecef        site position vector
%    site           site coordinate [geodetic latitude, longitude, altitude]
%    sigma          error of the observation
%    obstype        type of observation
%    Param          structure of constant parameter 
%
%   The call:
%      
%      [ Y0_EKF ] = ExtendedKalmanFilter( r2 , v2 , obs , Site , sigma , Param )
%
%   Returns:
%
%    Outputs         Description                    type [units]
%    Y0_EKF          initial state vector
%
%-Revisions
%
%   Version          Description                              Release
%     1.0.0          -                                        - 20-MAY-2019
%
%-References
%
%   - David Vallado
%
%-&========================================================================
%}

function [ Y0_EKF ] = ExtendedKalmanFilter( r2 , v2 , obs , Site , sigma , Param )
%%
global AuxParam eopdata

GM = Param.OrbitalMech.MainAttractor.GM;
re = Param.OrbitalMech.MainAttractor.R;
param = [ GM , re ];

eopdata = Param.OrbitalMech.eopdata;

% Model parameters
AuxParam = struct('Mjd_TT',0,'n',0,'m',0,'n_a',0,'m_a',0,'n_G',0,'m_G',0);

%%
rs = Site.Rs;
Lon = Site.lon;
Lat  = Site.lat;
sigma_range = sigma.range;
sigma_az = sigma.az;
sigma_el = sigma.el;

% dt = ( obs( 2 : end , 1 ) - obs( 1 : end - 1 , 1 ) ) .* ( 24 .* 3600 );

[ obs_row , ~ ] = size( obs );
nobs = obs_row;
n2 = 2;%round(nobs/2);

%%
Y0_apr = [r2;v2];

%%
Mjd0 = obs(1,1) - 1 / ( 24 * 3600 );

%%
Mjd_UTC = obs(n2,1);
% [UT1_UTC,TAI_UTC,x_pole,y_pole,ddpsi,ddeps] = IERS_o(eopdata,Mjd_UTC,'l');
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Mjd_UTC,'l');
[UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);

AuxParam.Mjd_TT = Mjd_UTC + TT_UTC/86400;
AuxParam.n      = 10;
AuxParam.m      = 10;
AuxParam.n_a    = 10;
AuxParam.m_a    = 10;
AuxParam.n_G    = 10;
AuxParam.m_G    = 10;

% n_eqn  = 6;

% options = rdpset('RelTol',1e-13,'AbsTol',1e-16);
% [~,yout] = radau(@Accel,[0 -(obs(n2,1)-Mjd0)*86400],Y0_apr,options,param);
options = odeset( ...
    'AbsTol'     , 2.22045e-14  ,...
    'RelTol'     , 2.22045e-14  );
[~,yout] = ode113(@Accel,[0 -(obs(n2,1)-Mjd0)*86400],Y0_apr,options,param);
Y = yout(end,:)';

P = zeros(6);
  
for i=1:3
    P(i,i)=1e8;
end
for i=4:6
    P(i,i)=1e3;
end

yPhi = zeros(42,1);
Phi  = zeros(6);

% Measurement loop
t = 0;

for i=1:nobs
    Rs = rs( : , i );
    lon = Lon( i );
    lat = Lat( i );
    
    % Previous step
    t_old = t;
    Y_old = Y;
    
    % Time increment and propagation
    Mjd_UTC = obs(i,1);                     % Modified Julian Date
    t       = (Mjd_UTC-Mjd0)*86400;         % Time since epoch [s]
    
%     [UT1_UTC,TAI_UTC,x_pole,y_pole,ddpsi,ddeps] = IERS_o(eopdata,Mjd_UTC,'l');
    [x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,Mjd_UTC,'l');
    [UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);
    Mjd_TT = Mjd_UTC + TT_UTC/86400;
    Mjd_UT1 = Mjd_TT + (UT1_UTC-TT_UTC)/86400;
    AuxParam.Mjd_UTC = Mjd_UTC;
    AuxParam.Mjd_TT = Mjd_TT;
        
    for ii=1:6
        yPhi(ii) = Y_old(ii);
        for j=1:6  
            if (ii==j) 
                yPhi(6*j+ii) = 1; 
            else
                yPhi(6*j+ii) = 0;
            end
        end
    end
    
    yPhi = DEInteg(@VarEqn,0,t-t_old,1e-13,1e-6,42,yPhi);
%     yPhi = DEInteg(@VarEqn,0,t-t_old,1e-13,1e-6,42,yPhi);
    
    % Extract state transition matrices
    for j=1:6
        Phi(:,j) = yPhi(6*j+1:6*j+6);
    end
    
%     [~,yout] = radau(@Accel,[0 t-t_old],Y_old,options);
    [~,yout] = ode113(@Accel,[0 t-t_old],Y_old,options,param);
    Y = yout(end,:)';
    
    % Topocentric coordinates
    theta = gmst(Mjd_UT1);                    % Earth rotation
    U = R_z(theta);
    r = Y(1:3);
    
    LT = LTCMatrix(lon,lat);
    s = LT*(U*r-Rs);                          % Topocentric position [m]
    
    % Time update
    P = TimeUpdate(P, Phi);
        
    % Azimuth and partials
    [Azim, Elev, dAds, dEds] = AzElPa(s);     % Azimuth, Elevation
    dAdY = [dAds*LT*U,zeros(1,3)];
    
    % Measurement update
    [K, Y, P] = MeasUpdate ( Y, obs(i,2), Azim, sigma_az, dAdY, P, 6 );
    
    % Elevation and partials
    r = Y(1:3);
    s = LT*(U*r-Rs);                          % Topocentric position [m]
    [Azim, Elev, dAds, dEds] = AzElPa(s);     % Azimuth, Elevation
    dEdY = [dEds*LT*U,zeros(1,3)];
    
    % Measurement update
    [K, Y, P] = MeasUpdate ( Y, obs(i,3), Elev, sigma_el, dEdY, P, 6 );
    
    % Range and partials
    r = Y(1:3);
    s = LT*(U*r-Rs);                          % Topocentric position [m]
    Dist = norm(s); dDds = (s/Dist)';         % Range
    dDdY = [dDds*LT*U,zeros(1,3)];
    
    % Measurement update
    [K, Y, P] = MeasUpdate ( Y, obs(i,4), Dist, sigma_range, dDdY, P, 6 );
end

% [UT1_UTC,TAI_UTC,x_pole,y_pole,ddpsi,ddeps] = IERS_o(eopdata,obs(nobs,1),'l');
[x_pole,y_pole,UT1_UTC,LOD,dpsi,deps,dx_pole,dy_pole,TAI_UTC] = IERS(eopdata,obs(nobs,1),'l');
[UT1_TAI, UTC_GPS, UT1_GPS, TT_UTC, GPS_UTC] = timediff(UT1_UTC, TAI_UTC);
Mjd_TT = Mjd_UTC + TT_UTC/86400;
AuxParam.Mjd_UTC = Mjd_UTC;
AuxParam.Mjd_TT = Mjd_TT;

% [~,yout] = radau(@Accel,[0 -(obs(end,1)-obs(1,1))*86400],Y,options);
[~,yout] = ode113(@Accel,[0 -(obs(end,1)-obs(1,1))*86400],Y,options,param);
Y0_EKF = yout(end,:)';

end