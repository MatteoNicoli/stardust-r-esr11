%--------------------------------------------------------------------------
%
% Inputs:
% Topocentric Right Ascension (Degrees)
% Topocentric Declination Angle (Degrees)
% Lat (Site Latitude in degrees -90:90 -> S(-) N(+))
% Lon (Site Longitude in degrees -180:180 W(-) E(+))
% JD
%
% Outputs:
% Local Azimuth Angle   (degrees)
% Local Elevation Angle (degrees)
%
%--------------------------------------------------------------------------
function [Az El] = RaDec2AzEl(Ra,Dec,lat,lon,JD)
T_UT1 = (JD-2451545)/36525;
ThetaGMST = 67310.54841 + (876600*3600 + 8640184.812866)*T_UT1 ...
+ .093104*(T_UT1^2) - (6.2*10^-6)*(T_UT1^3);
ThetaGMST = mod((mod(ThetaGMST,86400*(ThetaGMST/abs(ThetaGMST)))/240),360);
ThetaLST = ThetaGMST + lon;

LHA = mod(ThetaLST - Ra,360);

El = asind(sind(lat)*sind(Dec)+cosd(lat)*cosd(Dec)*cosd(LHA));

Az = mod(atan2(-sind(LHA)*cosd(Dec)/cosd(El),...
    (sind(Dec)-sind(El)*sind(lat))/(cosd(El)*cosd(lat)))*(180/pi),360);

