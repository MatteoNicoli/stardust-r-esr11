function [ State_ECI ] = uplanetECI( ET , BodyPar , GM )

mjd2000 = ET ./ ( 24 * 3600 );
switch BodyPar.ID
    case { 301 }
        [xP, vP] = ephMoon( mjd2000 );
        State_ECI = [ xP' ; vP' ];
        
    case { 10 }
        [ kep ] = uplanet( mjd2000 , 3 );
        [ State_se ] = kep2car( kep , kep( 6 ) , GM );
        State_ICRF = - State_se;
        [ State_ECI ] = ECLIPJ2000_2_J2000( State_ICRF );
        
end

end