function [ State_ECI ] = ECLIPJ2000_2_J2000( State_ICRF )

A = [ 1 ,                       0 ,                         0 ;
      0 , cosd( 23.4392911111111 ) , - sind( 23.4392911111111 ) ;
      0 , sind( 23.4392911111111 ) ,   cosd( 23.4392911111111 ) ];
  
A_t = [ A          , zeros( 3 ) ;
        zeros( 3 ) ,          A ];

State_ECI = A_t * State_ICRF;

end