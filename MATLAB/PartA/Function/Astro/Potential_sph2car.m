function [ F_car ] = Potential_sph2car( F_sph , r_vet )

[ r ] = car2sph( r_vet( 1 ) , r_vet( 2 ) , r_vet( 3 ) );

F_sph_r = F_sph( 1 );
F_sph_phi = F_sph( 2 );
F_sph_lambda = F_sph( 3 );

F_car_I = ( F_sph_r ./ r - r_vet( 3 ) .* F_sph_phi ./ ( r.^2 .*sqrt( r_vet( 1 ).^2 + r_vet( 2 ).^2 ) ) ) .* r_vet( 1 ) - F_sph_lambda .* r_vet( 2 ) ./ ( r_vet( 1 ).^2 + r_vet( 2 ).^2 );
F_car_J = ( F_sph_r ./ r - r_vet( 3 ) .* F_sph_phi ./ ( r.^2 .*sqrt( r_vet( 1 ).^2 + r_vet( 2 ).^2 ) ) ) .* r_vet( 2 ) + F_sph_lambda .* r_vet( 1 ) ./ ( r_vet( 1 ).^2 + r_vet( 2 ).^2 );
F_car_K = ( F_sph_r ./ r ) .* r_vet( 3 ) + F_sph_phi .* sqrt( r_vet( 1 ).^2 + r_vet( 2 ).^2 ) ./ r;

F_car = [ F_car_I ; F_car_J ; F_car_K ];

end