%% Inizialization.
clear all;
close all;
clc;

format longg
% format short

%% Attiveted all the subfolder.
% Installing;

%% Load the Data.
Data_Model;

%% Model.
Run_Model;

%% Plots.
Plot_Model;

%% Save.
% The function below (line 22 or 23) save the "base" workspace and all the 
% figure ploted in a new specific folder.
% SaveOverall('PartA')

%% De-Attiveted all the subsfolder.
% UnInstalling;