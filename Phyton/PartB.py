# -*- coding: utf-8 -*-
"""
Created on Sun Jun 16 23:18:17 2019

@author: matte
"""

# USAGE
# python distance_to_camera.py

# import the necessary packages
from imutils import paths
import numpy as np
import imutils
import cv2

###############################################################################
# function computes the centroid
def find_centroid(image):
    # calculate moments of binary image
    M = cv2.moments(image)
     
    # calculate x,y coordinate of center
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    
    return cX, cY;

# the function calculates the distance of the object
def distance_to_camera(knownWidth, focalLength, perWidth):
	# compute and return the distance from the maker to the camera
	return (knownWidth * focalLength) / perWidth

###############################################################################
# initialize the known object size, focal lenght and pixels size
KNOWN_WIDTH = 1.66    
FOCAL_LENGHT =0.00635
PIXELS_SIZE = 0.0000096

# name of the imges file
imagePath = "images/image3.png"

# read image
img = cv2.imread(imagePath)

# pixels dimension of the image (height,)width
height, width, channels = img.shape

# min. dimension of the image
min_dimimg = min(height,width)

# max. luminosity of the overall pixels
max_bp = 255 * height * width

###############################################################################
# convert the image to grayscale, blur it, and detect edges
gray1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gray2 = cv2.GaussianBlur(gray1, (5, 5), 0)

# number of pixel different from black
bw = sum(sum(gray2>0))

# Overall luminosity of the image
sum_gray2 = sum(sum(gray2))

# ration from umber of pixel different from black and Overall luminosity of the image
bw_sum = bw/sum_gray2*1000

#edged = cv2.Canny(gray2, 35, 1)

# linear regression to get the threshold of the image
m = (200-1)/(3680-3580)
q = 1 - m * 3580
y = int(round(m*bw_sum+q))

# contour of the object in the image more enlightened
edged_1 = cv2.Canny(gray2, 35, y)

###############################################################################
# dilate the contour
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
dilated = cv2.dilate(edged_1, kernel)

# extrapolate max blob in the image
cnts1 = cv2.findContours(dilated.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts1)
c = max(cnts, key = cv2.contourArea)
marker = cv2.minAreaRect(c)

###############################################################################
# compute the centroid
cX, cY = find_centroid(c)

# Object distance
dist = distance_to_camera(KNOWN_WIDTH/marker[1][1], FOCAL_LENGHT, PIXELS_SIZE)

###############################################################################
# print centroid coordinates
print('Pixel coordinates of center of mass: x=', cX, ' y=' , cY)

# print object distance
print('Object distance: ' ,dist, 'm')

###############################################################################
# draw the centroid
cv2.line(img,(cX-int(min_dimimg/10),cY),(cX+int(min_dimimg/10),cY),(0,0,255),2)
cv2.line(img,(cX,cY-int(min_dimimg/10)),(cX,cY+int(min_dimimg/10)),(0,0,255),2)

# draw the box contained the main object
box1 = cv2.cv.BoxPoints(marker) if imutils.is_cv2() else cv2.boxPoints(marker)
box = np.int0(box1)
cv2.drawContours(img, [box], -1, (0, 255, 0), 2)

###############################################################################
# display the image
cv2.imshow("Image1",img)
#cv2.imshow("Image2",edged_1)
#cv2.imshow("Image3",dilated)
cv2.waitKey(0)
    